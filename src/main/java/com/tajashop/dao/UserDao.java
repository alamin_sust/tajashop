package com.tajashop.dao;

import com.tajashop.connection.Database;
import com.tajashop.web.util.EncryptionUtil;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by md_al on 08-Dec-17.
 */
@Repository
public class UserDao {
    public boolean login(String username, String password, HttpSession session) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            Statement stmt = db.connection.createStatement();

            if (username != null && password != null) {

                String query = "select * from user where username = '" + username + "'";
                ResultSet rs = stmt.executeQuery(query);
                if (rs.next() == true && EncryptionUtil.matchWithSecuredHash(password, rs.getString("password"))) {
                    session.setAttribute("userId", rs.getString("id"));
                    session.setAttribute("username", username);
                    session.setAttribute("loginMsg", "Successfully Logged in!");
                    db.close();
                    return true;
                } else {
                    session.setAttribute("errorMsg", "Username and Password doesn't match. Try again.");
                    return false;
                }
            }
        } catch (Exception e) {
        } finally {
            db.close();
        }

        return false;
    }

    @Transactional
    public boolean signup(String username, String password, String password2, String email, String phone, String address, HttpSession session) throws SQLException {

        boolean isUsernameExists = usernameExists(username, session);

        Database db = new Database();
        db.connect();
        try {
            Statement stmt = db.connection.createStatement();

            if (username != null && password != null && password2 != null && email != null && phone != null
                    && address != null && !isUsernameExists && password.equals(password2)) {

                String hashedPass = EncryptionUtil.generateSecuredHash(password);
                String query = "insert into user (username,password, email, phone, address) " +
                        " VALUES ('" + username + "','" + hashedPass + "','" + email + "','" + phone + "','" + address + "')";
                stmt.executeUpdate(query);

                session.setAttribute("userId", getUserIdByUsername(username));
                session.setAttribute("username", username);
                session.setAttribute("signupMsg", "Successfully Signed Up!");
                db.close();
                return true;
            }
        } catch (Exception e) {
        } finally {
            db.close();
        }
        return false;
    }

    private String getUserIdByUsername(String username) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            Statement stmt = db.connection.createStatement();
            String query = "select id from user where username = '" + username + "'";
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            return rs.getString("id");

        } catch (Exception e) {

        } finally {
            db.close();
        }
        return null;
    }

    private boolean usernameExists(String username, HttpSession session) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            Statement stmt = db.connection.createStatement();
            String query = "select count(*) as cnt from user where username = '" + username + "'";
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            if(rs.getInt("cnt")!=0) {
                session.setAttribute("signupMsg", "Username already exists");
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {

        } finally {
            db.close();
        }
        return true;
    }

    public void setPass(String username, String hashedPass) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            Statement stmt = db.connection.createStatement();
            String query = "update user set password='" + hashedPass + "' where username = '" + username + "'";
            stmt.executeUpdate(query);
        } catch (Exception e) {

        } finally {
            db.close();
        }
    }
}
