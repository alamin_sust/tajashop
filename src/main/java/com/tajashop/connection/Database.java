/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tajashop.connection;

import com.mysql.jdbc.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

    public Connection connection = null;

    private final String USER_NAME = "root";
    private final String PASSWORD = "2011331055";
    private final String DB_URL = "jdbc:mysql://localhost:3306/tajashop?zeroDateTimeBehavior=convertToNull&useSSL=false";
    private final String DRIVER_NAME = "com.mysql.jdbc.Driver";

    public boolean connect() throws SQLException {
        try {
            Class.forName(DRIVER_NAME);
            connection = (Connection) DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //close();
        }
        return false;
    }

    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
