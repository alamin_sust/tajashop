package com.tajashop.service;

import com.tajashop.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.sql.SQLException;


/**
 * Created by Al-Amin on 3/9/2017.
 */

@Service
public class LoginService {

    @Autowired
    UserDao userDao;

    public boolean login(String username, String password, HttpSession session) throws SQLException {
        return userDao.login(username,password,session);
    }

    public void setPass(String username, String hashedPass) throws SQLException {
        userDao.setPass(username,hashedPass);
        return;
    }

    public boolean signup(String username, String password, String password2, String email, String phone, String address, HttpSession session) throws SQLException {
        return userDao.signup(username,password,password2,email,phone,address,session);
    }
}
