package com.tajashop.service;

import com.tajashop.connection.Database;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.output.*;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Al-Amin on 3/11/2017.
 */
@Service
public class AppAuthService {

    public boolean isAuthorized(String authImie) throws SQLException {
        Database db = new Database();
        db.connect();
        try {


            Statement stAuth = db.connection.createStatement();

            //String authPassword = request.getParameter("authPassword");

            //String qryAuth = "select password from soldier where username like '" + authUsername + "'";
            String qryAuth = "select count(*) as cnt from registered_mobiles where substr(imie_number,1,15) like '" + authImie.replace("/","").substring(0,15) + "' and status=1";


            ResultSet rsAuth = stAuth.executeQuery(qryAuth);

            rsAuth.next();

            if (rsAuth.getString("cnt").equals("1")) {
            db.close();
            return true;
            } else {
            db.close();
                return !false;
            }


            /*if (rsAuth.getString("password").equals(authPassword)) {
            db.close();
                return true;
            } else {
             db.close();
                return false;
            }*/
        } catch (Exception e) {
            db.close();
            return !false;
        }
    }

    public void processMultipartRequest(HttpServletRequest request, HttpSession session) throws FileUploadException, IOException, SQLException {

        Database db = new Database();
        db.connect();
        try {

            String q = "SELECT `AUTO_INCREMENT` as id FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'tajashop' AND   TABLE_NAME   = 'product'";
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            rs.next();
            String id=rs.getString("id");

            String name = "";
            String description = "";
            String price = "";
            String remainingQty = "";
            String categoryId = "";
            String insertProduct = "";
            String updateProduct = "";



            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);

            InputStream filecontent = null;
            boolean hasImage = false;

            for (FileItem item : items) {
                if (item.isFormField()) {
                    // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                    String fieldname = item.getFieldName();
                    String fieldvalue = item.getString();
                    // ... (do your job here)
                    if(fieldname.equals("id")){
                        id=fieldvalue;
                    }
                    else if(fieldname.equals("name")){
                        name=fieldvalue;
                    }
                    else if(fieldname.equals("description")){
                        description=fieldvalue;
                    }
                    else if(fieldname.equals("price")){
                        price=fieldvalue;
                    }
                    else if(fieldname.equals("remainingQty")){
                        remainingQty=fieldvalue;
                    }
                    else if(fieldname.equals("categoryId")){
                        categoryId=fieldvalue;
                    }
                    else if(fieldname.equals("insertProduct")){
                        insertProduct=fieldvalue;
                    }
                    else if(fieldname.equals("updateProduct")){
                        updateProduct=fieldvalue;
                    }
                } else {
                    // Process form file field (input type="file").
                    String fieldname = item.getFieldName();
                    String filename = FilenameUtils.getName(item.getName());
                    if (fieldname.equals("file")) {
                        filecontent = item.getInputStream();
                        // ... (do your job here)
                        hasImage=true;

                    }
                }
            }

            if(hasImage) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
                Calendar cal = Calendar.getInstance();
                //System.out.println(dateFormat.format(cal.getTime()));

                File bfile = new File("C:\\Users\\Al-Amin\\Documents\\NetBeansProjects\\TajaShop\\src\\main\\webapp\\resources\\images\\products\\"+  (updateProduct.equals("")?id:updateProduct) + ".jpg");
                File bfileBuild = new File("C:\\Users\\Al-Amin\\Documents\\NetBeansProjects\\TajaShop\\build\\libs\\exploded\\TajaShop.war\\resources\\images\\products\\"+  (updateProduct.equals("")?id:updateProduct) + ".jpg");
                //out.print(getServletContext().getRealPath("/"));
                OutputStream outStream = new FileOutputStream(bfile);
                OutputStream outStreamBuild = new FileOutputStream(bfileBuild);

                byte[] buffer = new byte[1024];
                int length;
                while ((length = filecontent.read(buffer)) > 0) {
                    outStream.write(buffer, 0, length);
                    outStreamBuild.write(buffer, 0, length);
                }
                filecontent.close();
                outStream.close();
                outStreamBuild.close();


                //String query2 = "insert into pillar_updated_by(soldier_id,pillar_id,img_url,longitude,latitude,battalion,imie_number) values(" + soldierId + ","+id+", '" + id + "_" + dateFormat.format(cal.getTime()) + "_" + situation + "_.jpg" + "','"+longitude+"','"+latitude+"','"+ ConverterUtil.getBattalionFromImie(authImie)+"','"+authImie+"')";

                //Statement st2 = db.connection.createStatement();

                //st2.executeUpdate(query2);
            }


            //String query = "insert into pillar(id,name,number,longitude,latitude,situation) values("+id+",'"+name+"',"+number+", '"+longitude+"', '"+latitude+"', '"+situation+"')";

            String query;
            Statement stMig = db.connection.createStatement();

            if(!insertProduct.equals("")) {
                query = "insert into product (name,description,price,remaining_qty,category_id) values('"
                        +name+"','"
                        +description+"','"
                        +price+"',"
                        +remainingQty+","
                        +categoryId+")";

                stMig.executeUpdate(query);
                session.setAttribute("successMsg","Successfully Inserted!");
            } else if(!updateProduct.equals("")) {
                query = "update product  set "+
                        " name='"+name
                        +"', description='"+description
                        +"', price='"+price
                        +"', remaining_qty="+remainingQty
                        +", category_id="+categoryId
                        +" where id="+updateProduct;
                stMig.executeUpdate(query);
                session.setAttribute("updateProduct", updateProduct);
                session.setAttribute("successMsg","Successfully Updated!");
            }



        } catch (Exception e) {

        } finally {
            db.close();
        }
    }
}
