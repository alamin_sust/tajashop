package com.tajashop.service;

import com.tajashop.dao.ProductDao;
import com.tajashop.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by md_al on 09-Dec-17.
 */
@Service
public class ProductService {
    @Autowired
    private ProductDao productDao;

    public List<Product> getProducts(String category, String subCategory) {
        return productDao.getProducts(category,subCategory);
    }
}
