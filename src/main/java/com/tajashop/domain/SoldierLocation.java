package main.java.com.tajashop.domain;

import java.io.Serializable;

/**
 * Created by Al-Amin on 3/10/2017.
 */
public class SoldierLocation implements Serializable{
    private static final long serialVersionUID = 1L;
    private int id;
    private int soldierId;
    private String longitude;
    private String latitude;
    private String dateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSoldierId() {
        return soldierId;
    }

    public void setSoldierId(int soldierId) {
        this.soldierId = soldierId;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
