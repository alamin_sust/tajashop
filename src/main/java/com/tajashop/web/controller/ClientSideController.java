package com.tajashop.web.controller;

import com.tajashop.service.AppAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

/**
 * Created by md_al on 24-Dec-17.
 */
@Controller
public class ClientSideController {

    @Autowired
    AppAuthService appAuthService;

    @GetMapping("/sendFirebaseMsg")
    public String sendFirebaseMsgGet() {
        return "sendFirebaseMsg";
    }
    @PostMapping("/sendFirebaseMsg")
    public String sendFirebaseMsgPost() {
        return "sendFirebaseMsg";
    }

    @GetMapping("/deliveryBoyLogin")
    public String deliveryBoyLoginGet() {
        return "deliveryBoyLogin";
    }
    @PostMapping("/deliveryBoyLogin")
    public String deliveryBoyLoginPost(HttpServletRequest request) throws SQLException {
        if(!appAuthService.isAuthorized(request.getParameter("authImie"))) {
            return "appAuthFailPage";
        }
        return "deliveryBoyLogin";
    }

    @GetMapping("/deliveryBoyRegister")
    public String deliveryBoyRegisterGet() {
        return "deliveryBoyRegister";
    }
    @PostMapping("/deliveryBoyRegister")
    public String deliveryBoyRegisterPost() {
        return "deliveryBoyRegister";
    }

    @PostMapping("/updateDeliveryBoyLocation")
    public String postViewUpdateSoldierLocation(HttpServletRequest request) throws SQLException {
        if(!appAuthService.isAuthorized(request.getParameter("authImie"))) {
            return "appAuthFailPage";
        }

        return "updateDeliveryBoyLocation";
    }
}
