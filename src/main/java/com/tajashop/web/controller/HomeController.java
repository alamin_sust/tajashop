package com.tajashop.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by Al-Amin on 3/8/2017.
 */
@Controller
public class HomeController {

    private static final String REDIRECT_HOME = "redirect:/home";
    private static final String HOME = "home";

    @GetMapping("/")
    public String goHome() {
        return REDIRECT_HOME;
    }

    @GetMapping("/home")
    public String home() {
        return HOME;
    }
}
