package com.tajashop.web.controller;

import com.tajashop.service.AppAuthService;
import com.tajashop.service.ProductService;
import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by md_al on 09-Dec-17.
 */
@Controller
public class OtherController {

    @Autowired
    private AppAuthService appAuthService;

    private static final String REDIRECT = "redirect:/";
    private static final String PRODUCTS = "products";

    @GetMapping("/products")
    public String productsGet(Model model,
                              @RequestParam(value = "category", required = false) String category,
                              @RequestParam(value = "subCategory", required = false) String subCategory) {
        //model.addAttribute("productList", productService.getProducts(category,subCategory));
        return PRODUCTS;
    }

    @PostMapping("/products")
    public String prductsPost() {
        return PRODUCTS;
    }

    @GetMapping("/addProduct")
    public String addProductGet() {
        return "addProduct";
    }

    @PostMapping("/addProduct")
    public String addProductPost(HttpServletRequest request, HttpSession session) throws SQLException, IOException, FileUploadException {
        appAuthService.processMultipartRequest(request, session);
        return "addProduct";
    }

    @GetMapping("/updateUser")
    public String updateUserGet() {
        return "updateUser";
    }

    @PostMapping("/updateUser")
    public String updateUserPost() {
        return "updateUser";
    }

    @GetMapping("/checkout")
    public String checkoutGet() {
        return "checkout";
    }

    @PostMapping("/checkout")
    public String checkoutPost() {
        return "checkout";
    }

    @GetMapping("/faqs")
    public String faqsGet() {
        return "faqs";
    }

    @GetMapping("/terms")
    public String termsGet() {
        return "terms";
    }

    @GetMapping("/privacy")
    public String privacyGet() {
        return "privacy";
    }

    @GetMapping("/about")
    public String aboutGet() {
        return "about";
    }

    @GetMapping("/single")
    public String singleGet() {
        return "single";
    }

    @GetMapping("/somethingWentWrong")
    public String somethingWentWrongGet() {
        return "somethingWentWrong";
    }

    @GetMapping("/orderStatus")
    public String orderStatusGet() {
        return "orderStatus";
    }
    @PostMapping("/orderStatus")
    public String orderStatusPost() {
        return "orderStatus";
    }
    @GetMapping("/orders")
    public String ordersGet() {
        return "orders";
    }
    @PostMapping("/orders")
    public String ordersPost() {
        return "orders";
    }
}