package com.tajashop.web.controller;

import com.tajashop.connection.Database;
import com.tajashop.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Al-Amin on 3/9/2017.
 */
@Controller
public class LoginController {

    @Autowired
    LoginService loginService;

    private static final String LOGIN = "login";
    private static final String REDIRECT_HOME = "redirect:/home";

    @GetMapping("/login")
    public String loadLoginPage(HttpSession session) {
        if (session.getAttribute("userId") != null) {
            return REDIRECT_HOME;
        }
        session.setAttribute("loginMsg", null);
        return LOGIN;
    }

    @PostMapping("/login")
    public String login(@RequestParam(value = "username", required = false) String username,
                        @RequestParam(value = "password", required = false) String password,
                        @RequestParam(value = "logout", required = false) String logout,
                        HttpSession session) throws SQLException {

        /*pass reset from login page*/
        /*String hashedPass = EncryptionUtil.generateSecuredHash(password);
        if(username!=null && hashedPass !=null) {
            loginService.setPass(username, hashedPass);
        }*/

        session.setAttribute("loginMsg", null);

        if (logout != null) {
            session.setAttribute("userId", null);
            session.setAttribute("username", null);
            return LOGIN;
        }


        if (loginService.login(username, password, session) /*&& MacBindingUtil.isAuthorizedMac()*/) {
            return REDIRECT_HOME;
        }

        return LOGIN;
    }


    @GetMapping("/signup")
    public String loadSignupPage(HttpSession session) {
        if (session.getAttribute("userId") != null) {
            return REDIRECT_HOME;
        }
        session.setAttribute("loginMsg", null);
        return LOGIN;
    }

    @PostMapping("/signup")
    public String signup(@RequestParam(value = "username", required = false) String username,
                                        @RequestParam(value = "password", required = false) String password,
                                        @RequestParam(value = "password2", required = false) String password2,
                                        @RequestParam(value = "email", required = false) String email,
                                        @RequestParam(value = "phone", required = false) String phone,
                                        @RequestParam(value = "address", required = false) String address,
                                        HttpSession session) throws SQLException {


        if (loginService.signup(username, password, password2, email, phone, address, session) /*&& MacBindingUtil.isAuthorizedMac()*/) {
            return REDIRECT_HOME;
        }
        return LOGIN;
    }


}
