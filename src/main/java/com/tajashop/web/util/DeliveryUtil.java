package com.tajashop.web.util;

import com.tajashop.connection.Database;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Random;

/**
 * Created by md_al on 23-Dec-17.
 */
public class DeliveryUtil {
    public static int getDeliveryBoyId(int deliveryType, String deliveryAddress) {
        return 2;
    }

    public static String getPaymentToken() {

        String ret = "";

        Random rand = new Random();

        for(int i=0;i<8;i++) {
            int now= Math.abs(rand.nextInt()%36);

            if(now<10) {
                ret+=String.valueOf(now);
            }
            else {
                ret+=(char)('A'+(now-10));
            }
        }

        return ret;

    }

    public static void sendPushNotification(int deliveryBoyId, String title) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            Statement st = db.connection.createStatement();
            String q = "select * from order_ where delivery_boy_id="+deliveryBoyId+" and status='running' ORDER by id DESC ";
            ResultSet rs = st.executeQuery(q);

            rs.next();

            int ONE_HOUR = (1000*60*60);
            int time;
            String typeStr;

            if(rs.getString("delivery_type").equals("1")) {
                time = ONE_HOUR;
                typeStr = "Quick Delivery Needed";
            } else if(rs.getString("delivery_type").equals("2")) {
                time = 6*ONE_HOUR;
                typeStr = "Normal Delivery Needed";
            } else {
                time = 24*ONE_HOUR;
                typeStr = "Economy Delivery Needed";
            }

            Timestamp deadline = new Timestamp(rs.getTimestamp("order_time").getTime() + time);



            Statement st2 = db.connection.createStatement();
            String q2 = "select * from delivery_boy where id="+deliveryBoyId;
            ResultSet rs2 = st2.executeQuery(q2);

            rs2.next();


            HttpURLConnection httpcon = (HttpURLConnection) ((new URL("https://fcm.googleapis.com/fcm/send").openConnection()));
            httpcon.setDoOutput(true);
            httpcon.setRequestProperty("Content-Type", "application/json");
            httpcon.setRequestProperty("Authorization", "key=AAAA5axNW9Y:APA91bHwHWbZMJ9DZ-tXidTpKlAnVtsy6cQWTlUWn1y2HLSxS2YT9TWyaVQZ1ca_RpVYUOwwwWCAin-htrUHnL4Qg5HSVHThj2xMwDeJe_JyW9__ug5ir_0v48O1pKV8hlgm8jfS7lfC");
            httpcon.setRequestMethod("POST");
            httpcon.connect();
            System.out.println("Connected!");

            String contentStr  = "{   \"to\": \"" + rs2.getString("fcm_id") + "\",   \"\"data\"\":{\"\"title\"\": \"\""+title+"\"\", \"\"message\"\": \"\"" + typeStr + "\"\", \"\"name\"\": \"\""+rs.getString("name")+"\"\", \"\"mobile\"\": \"\"" + rs.getString("mobile") + "\"\", \"\"address\"\": \"\"" + rs.getString("address") + "\"\", \"\"deadline\"\": \"\"" + deadline.toString() + "\"\", \"\"products\"\": \"\"" + rs.getString("product_info") + "\"\", \"\"pathId\"\": \"\"" + rs.getString("id") + "\"\", \"\"grandTotal\"\": \"\"" + rs.getString("grand_total") + "\"\"} }";

            System.out.println(contentStr);

            byte[] outputBytes = contentStr.getBytes("UTF-8");
            OutputStream os = httpcon.getOutputStream();
            System.out.println(outputBytes);
            os.write(outputBytes);
            os.close();

            // Reading response
            InputStream input = httpcon.getInputStream();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
                for (String line; (line = reader.readLine()) != null;) {
                    System.out.println(line);
                }
            }

            System.out.println("Http POST request sent!");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            db.close();
        }
    }
}
