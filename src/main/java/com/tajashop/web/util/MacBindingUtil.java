package com.tajashop.web.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Created by Al-Amin on 3/13/2017.
 */
public class MacBindingUtil {
        private static String authorizedMacs[] = {"08-D4-0C-DE-12-46"};
        private static int allowedMacs = 1;
        public static String getMacAddress(){

            InetAddress ip;
            try {

                ip = InetAddress.getLocalHost();
                System.out.println("Current IP address : " + ip.getHostAddress());

                NetworkInterface network = NetworkInterface.getByInetAddress(ip);

                byte[] mac = network.getHardwareAddress();

                System.out.print("Current MAC address : ");

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < mac.length; i++) {
                    sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
                }
                //System.out.println(sb.toString());
                return sb.toString();

            } catch (UnknownHostException e) {

                e.printStackTrace();

            } catch (SocketException e){

                e.printStackTrace();

            }
            return null;
        }

    public static boolean isAuthorizedMac() {
        for(int i=0; i<allowedMacs; i++) {
            if (MacBindingUtil.getMacAddress().equals(authorizedMacs[i])) {
                return true;
            }
        }
        return false;
    }
}
