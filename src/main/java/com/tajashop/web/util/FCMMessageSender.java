package com.tajashop.web.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import com.tajashop.connection.Database;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by md_al on 02-Jan-18.
 */
/**
 * @author athakur
 */
public class FCMMessageSender {

    public static final String FCM_URL = "https://fcm.googleapis.com/fcm/send";
    public static final String FCM_SERVER_API_KEY    = "AAAA5axNW9Y:APA91bHwHWbZMJ9DZ-tXidTpKlAnVtsy6cQWTlUWn1y2HLSxS2YT9TWyaVQZ1ca_RpVYUOwwwWCAin-htrUHnL4Qg5HSVHThj2xMwDeJe_JyW9__ug5ir_0v48O1pKV8hlgm8jfS7lfC";
    public static void sendPushNotification(int deliveryBoyId, String title) throws SQLException {
        int responseCode = -1;
        String responseBody = null;
        Database db = new Database();
        db.connect();
        ResultSet rs;
        ResultSet rs2 = null;
        try
        {
            System.out.println("Sending FCM request");





                Statement st = db.connection.createStatement();
                String q = "select * from order_ where delivery_boy_id="+deliveryBoyId+" and status='running' ORDER by id desc";
                rs = st.executeQuery(q);

                rs.next();

                int ONE_HOUR = (1000*60*60);
                int time;
                String typeStr;

                if(rs.getString("delivery_type").equals("1")) {
                    time = ONE_HOUR;
                    typeStr = "Quick Delivery Needed";
                } else if(rs.getString("delivery_type").equals("2")) {
                    time = 6*ONE_HOUR;
                    typeStr = "Normal Delivery Needed";
                } else {
                    time = 24*ONE_HOUR;
                    typeStr = "Economy Delivery Needed";
                }

                Timestamp deadline = new Timestamp(rs.getTimestamp("order_time").getTime() + time);



                Statement st2 = db.connection.createStatement();
                String q2 = "select * from delivery_boy where id="+deliveryBoyId;
                rs2 = st2.executeQuery(q2);

                rs2.next();


            HashMap<String, String> dataMap = new HashMap<>();
            JSONObject payloadObject = new JSONObject();

            dataMap.put("\"title\"", "\""+title+"\"");
            dataMap.put("\"message\"", "\""+typeStr+"\"");
            dataMap.put("\"name\"", "\""+rs.getString("name")+"\"");
            dataMap.put("\"mobile\"", "\""+rs.getString("mobile")+"\"");
            dataMap.put("\"address\"", "\""+rs.getString("address")+"\"");
            dataMap.put("\"deadline\"", "\""+deadline.toString()+"\"");
            dataMap.put("\"products\"", "\""+rs.getString("product_info")+"\"");
            dataMap.put("\"pathId\"", "\""+rs.getString("id")+"\"");
            dataMap.put("\"grandTotal\"", "\""+rs.getString("grand_total")+"\"");

            JSONObject data = new JSONObject(dataMap);;
            payloadObject.put("data", data);
            payloadObject.put("to", rs2.getString("fcm_id"));




            byte[] postData = payloadObject.toString().getBytes();

            URL url = new URL(FCM_URL);
            HttpsURLConnection httpURLConnection = (HttpsURLConnection)url.openConnection();

            //set timeputs to 10 seconds
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setReadTimeout(10000);

            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestProperty("Content-Length", Integer.toString(postData.length));
            httpURLConnection.setRequestProperty("Authorization", "key="+FCM_SERVER_API_KEY);



            OutputStream out = httpURLConnection.getOutputStream();
            out.write(postData);
            out.close();
            responseCode = httpURLConnection.getResponseCode();
            //success
            if (responseCode == HttpStatus.SC_OK)
            {
                responseBody = convertStreamToString(httpURLConnection.getInputStream());
                System.out.println("FCM message sent : " + responseBody);
            }
            //failure
            else
            {
                responseBody = convertStreamToString(httpURLConnection.getErrorStream());
                System.out.println("Sending FCM request failed for regId: " + rs2.getString("fcm_id") + " response: " + responseBody);
            }
        }
        catch (IOException ioe)
        {
            System.out.println("IO Exception in sending FCM request. regId: " + rs2.getString("fcm_id"));
            ioe.printStackTrace();
        }
        catch (Exception e)
        {
            System.out.println("Unknown exception in sending FCM request. regId: " + rs2.getString("fcm_id"));
            e.printStackTrace();
        }finally {
            db.close();
        }
    }

    public static byte[] getPostData(String registrationId, int deliveryBoyId, String title) throws JSONException, SQLException {

        Database db = new Database();
        db.connect();
        ResultSet rs;
        ResultSet rs2;
        try {
        Statement st = db.connection.createStatement();
        String q = "select * from order_ where delivery_boy_id="+deliveryBoyId+" and status='running'";
        rs = st.executeQuery(q);

        rs.next();

        int ONE_HOUR = (1000*60*60);
        int time;
        String typeStr;

        if(rs.getString("delivery_type").equals("1")) {
            time = ONE_HOUR;
            typeStr = "Quick Delivery Needed";
        } else if(rs.getString("delivery_type").equals("2")) {
            time = 6*ONE_HOUR;
            typeStr = "Normal Delivery Needed";
        } else {
            time = 24*ONE_HOUR;
            typeStr = "Economy Delivery Needed";
        }

        Timestamp deadline = new Timestamp(rs.getTimestamp("order_time").getTime() + time);



        Statement st2 = db.connection.createStatement();
        String q2 = "select * from delivery_boy where id="+deliveryBoyId;
        rs2 = st2.executeQuery(q2);

        rs2.next();

        } finally {
            db.close();
        }


        HashMap<String, String> dataMap = new HashMap<>();
        JSONObject payloadObject = new JSONObject();

        dataMap.put("name", "Aniket!");
        dataMap.put("country", "India");

        JSONObject data = new JSONObject(dataMap);;
        payloadObject.put("data", data);
        payloadObject.put("to", registrationId);

        return payloadObject.toString().getBytes();
    }

    public static String convertStreamToString (InputStream inStream) throws Exception
    {
        InputStreamReader inputStream = new InputStreamReader(inStream);
        BufferedReader bReader = new BufferedReader(inputStream);

        StringBuilder sb = new StringBuilder();
        String line = null;
        while((line = bReader.readLine()) != null)
        {
            sb.append(line);
        }

        return sb.toString();
    }

}