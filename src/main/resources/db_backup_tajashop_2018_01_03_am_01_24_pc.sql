-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: tajashop
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `category_number` int(11) DEFAULT NULL,
  `subcategory_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Fresh Vegitable',1,0),(2,'Fresh Fruits',2,0),(3,'Meat & Poultry',3,0),(4,'Chicken',3,1),(5,'Mutton',3,2),(6,'Beef',3,3),(7,'Seafood & Fish',4,0),(8,'Grains',5,0),(9,'Whole Grains',5,1),(10,'Others',5,2),(11,'Desert & Baked',6,0),(12,'Frozen Foods',7,0),(13,'Spices',8,0),(14,'Dry & Canned',9,0);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_boy`
--

DROP TABLE IF EXISTS `delivery_boy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_boy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(110) DEFAULT NULL,
  `password` varchar(110) DEFAULT NULL,
  `name` varchar(110) DEFAULT NULL,
  `mobile` varchar(110) DEFAULT NULL,
  `details` varchar(1010) DEFAULT NULL,
  `fcm_id` varchar(1010) DEFAULT NULL,
  `path_status` int(11) DEFAULT NULL,
  `last_longitude` varchar(110) DEFAULT NULL,
  `last_latitude` varchar(110) DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `delivery_boy_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_boy`
--

LOCK TABLES `delivery_boy` WRITE;
/*!40000 ALTER TABLE `delivery_boy` DISABLE KEYS */;
INSERT INTO `delivery_boy` VALUES (1,'wearesexy','$2a$12$etxOouGbBpXmAy3HYfgQ/uJip88D28gcrraOv1Sgu3NzaAuCXykTG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-12-31 15:28:52'),(2,'deliveryBoy1','$2a$12$nlR1c2yOIkun3QWX3yOCcemQvrt652Mzo2gIRamfZhKzXp2sQh2T6',NULL,NULL,NULL,'coqGP_8aXXA:APA91bHkRvp7iYTAnwViurlQI3FsAeTlOXu7O2RWGm8nbdM73uBXu-JLs-EzJ0aXaJ9bOBJBN9mjONuOm_eHxbMLK1dOTavCgr-YT9gUptxm8ksugGlvV9XiaqYsZbhXtO7vd_rHquKR',0,'91.95314','24.91239','2018-01-02 19:16:29');
/*!40000 ALTER TABLE `delivery_boy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_boy_location`
--

DROP TABLE IF EXISTS `delivery_boy_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_boy_location` (
  `id` int(11) NOT NULL,
  `delivery_boy_id` int(11) DEFAULT NULL,
  `longitude` varchar(110) DEFAULT NULL,
  `latitude` varchar(110) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `path_id` int(11) DEFAULT NULL,
  `imie_number` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `delivery_boy_location_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_boy_location`
--

LOCK TABLES `delivery_boy_location` WRITE;
/*!40000 ALTER TABLE `delivery_boy_location` DISABLE KEYS */;
INSERT INTO `delivery_boy_location` VALUES (0,NULL,NULL,NULL,'2017-12-30 09:34:39',0,NULL),(1,2,'11.12','122.12','2017-12-31 15:32:14',1,'357558066933272'),(2,2,'11.12','122.12','2017-12-31 15:33:25',2,'357558066933272'),(3,2,'91.95009','24.90656','2017-12-31 15:34:06',3,'357558066933272'),(4,2,'91.95369','24.91289','2017-12-31 15:34:20',3,'357558066933272'),(5,2,'91.95351','24.9127','2017-12-31 15:34:42',3,'357558066933272'),(6,2,'91.95351','24.9127','2017-12-31 15:34:47',3,'357558066933272'),(7,2,'91.95009','24.90656','2017-12-31 15:36:18',4,'357558066933272'),(8,2,'91.95357','24.9127','2017-12-31 15:36:21',4,'357558066933272'),(9,2,'91.95358','24.91269','2017-12-31 15:39:09',4,'357558066933272'),(10,2,'91.9507','24.90729','2017-12-31 15:39:11',4,'357558066933272'),(11,2,'91.95358','24.91269','2017-12-31 15:39:15',4,'357558066933272'),(12,2,'91.95358','24.91269','2017-12-31 15:39:26',4,'357558066933272'),(13,2,'91.9507','24.90729','2017-12-31 15:39:30',5,'357558066933272'),(14,2,'91.95358','24.91269','2017-12-31 15:39:31',6,'357558066933272'),(15,2,'91.95362','24.9128','2017-12-31 15:39:43',7,'357558066933272'),(16,2,'91.95361','24.91276','2017-12-31 15:39:59',7,'357558066933272'),(17,2,'91.95356','24.91285','2018-01-02 18:51:04',28,'357558066933272'),(18,2,'91.95329','24.91254','2018-01-02 18:51:06',28,'357558066933272'),(19,2,'91.9533','24.91255','2018-01-02 18:51:07',28,'357558066933272'),(20,2,'91.95332','24.91257','2018-01-02 18:51:14',28,'357558066933272'),(21,2,'91.95332','24.91253','2018-01-02 18:51:50',28,'357558066933272'),(22,2,'91.95341','24.91249','2018-01-02 18:53:15',28,'357558066933272'),(23,2,'91.95335','24.91257','2018-01-02 18:54:16',28,'357558066933272'),(24,2,'91.95335','24.91257','2018-01-02 18:54:21',29,'357558066933272'),(25,2,'91.95335','24.91257','2018-01-02 18:54:34',29,'357558066933272'),(26,2,'91.95356','24.91285','2018-01-02 18:59:53',30,'357558066933272'),(27,2,'91.95346','24.91249','2018-01-02 18:59:56',30,'357558066933272'),(28,2,'91.9535','24.91255','2018-01-02 19:00:09',30,'357558066933272'),(29,2,'91.95363','24.91282','2018-01-02 19:03:18',33,'357558066933272'),(30,2,'91.95364','24.91282','2018-01-02 19:03:20',33,'357558066933272'),(31,2,'91.95356','24.91272','2018-01-02 19:05:02',33,'357558066933272'),(32,2,'91.95336','24.91256','2018-01-02 19:05:42',33,'357558066933272'),(33,2,'91.95312','24.91243','2018-01-02 19:05:52',33,'357558066933272'),(34,2,'91.95296','24.91234','2018-01-02 19:06:02',33,'357558066933272'),(35,2,'91.95311','24.91246','2018-01-02 19:06:12',33,'357558066933272'),(36,2,'91.95323','24.9125','2018-01-02 19:07:14',33,'357558066933272'),(37,2,'91.95337','24.91253','2018-01-02 19:09:03',33,'357558066933272'),(38,2,'91.95339','24.91272','2018-01-02 19:09:12',33,'357558066933272'),(39,2,'91.95325','24.91273','2018-01-02 19:09:42',33,'357558066933272'),(40,2,'91.9532','24.91262','2018-01-02 19:10:02',33,'357558066933272'),(41,2,'91.95342','24.91267','2018-01-02 19:10:34',33,'357558066933272'),(42,2,'91.95345','24.91266','2018-01-02 19:10:47',33,'357558066933272'),(43,2,'91.95345','24.91265','2018-01-02 19:10:54',32,'357558066933272'),(44,2,'91.95009','24.90656','2018-01-02 19:10:59',32,'357558066933272'),(45,2,'91.95308','24.91237','2018-01-02 19:16:18',34,'357558066933272'),(46,2,'91.95314','24.91239','2018-01-02 19:16:20',34,'357558066933272'),(47,2,'91.95314','24.91239','2018-01-02 19:16:29',34,'357558066933272');
/*!40000 ALTER TABLE `delivery_boy_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_`
--

DROP TABLE IF EXISTS `order_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `name` varchar(110) DEFAULT NULL,
  `address` varchar(1010) DEFAULT NULL,
  `mobile` varchar(110) DEFAULT NULL,
  `product_info` varchar(1010) DEFAULT NULL,
  `delivery_boy_id` int(11) DEFAULT NULL,
  `delivery_type` int(11) DEFAULT NULL,
  `order_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `payment_token` varchar(110) DEFAULT NULL,
  `grand_total` double DEFAULT NULL,
  `status` varchar(110) DEFAULT 'running',
  `path_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_`
--

LOCK TABLES `order_` WRITE;
/*!40000 ALTER TABLE `order_` DISABLE KEYS */;
INSERT INTO `order_` VALUES (1,0,'ert','etrt','ert','3-1_5-1_7-2_1-6',1,2,'2018-01-01 15:33:43','B-77-79-1533',888,'running',1),(2,0,'sdf','Dfasdfasd','fsd','3-1_5-1_7-2_1-6',1,3,'2018-01-01 15:33:43','2EAA-240D5',885,'running',2),(3,0,'uy','yu','yu','1-33',1,2,'2018-01-01 15:33:43','LEESHP0F',3371,'running',2),(4,0,'Al-Amin','Dhaka','01711389230','3-3_7-2',2,2,'2018-01-01 15:33:43','OEXB4195',360,'running',3),(5,0,'Al-Amin','SUST, Sylhet','01711389230','3-3_7-4',2,2,'2018-01-01 15:33:43','LBWX606D',586,'running',4),(6,2,'Shahad','jjdjd, skskks','99999','7-1_3-3',2,2,'2018-01-01 15:33:43','89PY6F25',247,'running',6),(7,2,'hrtyerty','4563456','56546','7-1_3-3',2,2,'2018-01-02 19:16:34','U4RAW2UY',247,'completed',6),(8,5,'Md. Al- Amin','SUST, Sylhet','2345','19-1_12-1_17-5',2,1,'2018-01-01 16:40:18','80WXBKEH',2578.2,'running',8),(9,5,'Sadi','Dhaka, BD','12121','19-1_12-1_17-5_15-1',2,1,'2018-01-01 18:39:07','FSPFIAYE',2601.2,'running',9),(10,2,'Shahad','Dhk BD','54534523','19-1_12-1_17-5_15-1_7-1',2,1,'2018-01-01 18:40:01','UAJ1Y03N',2714.2,'running',10),(11,2,'Hagu','Paad','1234','17-5_15-1',2,1,'2018-01-01 18:45:59','90O15LL7',258,'running',11),(12,2,'Hagu','Paad','102','17-5_15-1_19-4',2,1,'2018-01-01 18:50:08','2GK48BI7',266.8,'running',12),(13,2,'ertwert','ertwert','3425','17-5_15-1_19-4_5-1',2,2,'2018-01-01 19:03:33','PDDCDGNX',263.8,'running',13),(14,2,'werwe','ertwertwer','234534','17-6_15-1_19-4_5-1',2,3,'2018-01-01 19:13:07','0AZDBWRH',305.8,'running',14),(15,2,'ertewrt','dfgsd','34535','17-6_15-1_19-5_5-1',2,2,'2018-01-01 19:21:07','R711VE95',311,'running',15),(16,2,'rrrrrrrrrrrrr','rfffffffffffffffffffff','234123','17-6_15-12_19-5_5-2',2,1,'2018-01-01 19:25:36','I5JTZ3T4',571,'running',16),(17,2,'aaaaa','hhhhhhhhhh','8678567','7-1_3-3',2,3,'2018-01-02 13:40:24','P6IFAWLF',244,'running',17),(18,2,'loool','lul','34234123','7-1_3-3_4-7',2,2,'2018-01-02 14:32:15','PM0W9YFM',478,'running',18),(19,0,'eeeeeee','rrrrrrrrrr','5464356','7-2_3-3_4-7',2,3,'2018-01-02 17:50:57','XCEDYHSL',588,'running',19),(20,2,'zzzzzzzzzzzz','xxxxxxxxxxxx','54645','5-1_7-4',2,2,'2018-01-02 18:00:38','OWWFJOEB',459,'running',20),(21,2,'twerter','ertwrwer','5345','5-2_7-4',2,1,'2018-01-02 18:02:39','W5SEM12T',466,'running',21),(22,2,'iyuiyui','kghjk','6786','5-2_7-4',2,1,'2018-01-02 18:04:42','98DHELJJ',466,'running',22),(23,0,'ertert','gdfgsdfg','3453','5-2_7-6',2,1,'2018-01-02 18:09:35','543D5ZKU',692,'running',23),(24,2,'rrwe','rtyerty','098990789','7-6_3-1',2,1,'2018-01-02 18:15:27','3NN4M7WI',731,'running',24),(25,2,'fhdfgh','ghfghf','6456','7-6_3-2',2,1,'2018-01-02 18:17:01','OMUW3303',774,'running',25),(26,2,'Jubayer','Dashpara, Sylhet','54234534','7-7_3-2',2,1,'2018-01-02 18:41:10','Z2Z5MDIC',887,'running',26),(27,0,'Jub','Daspara','3454','7-8_3-2',2,1,'2018-01-02 18:44:10','JXJF43FE',1000,'running',27),(28,0,'Jubayer','Daspara, Sylhet','01142423','7-8_3-2_12-1',2,3,'2018-01-02 18:48:59','2WGS9IZU',3333,'running',28),(29,0,'Al-Amin','Dhk','52347675','3-2_12-1',2,2,'2018-01-02 18:50:23','V5185OVN',2432,'running',29),(30,0,'alamin','Sylhet','5463456','3-2_12-4',2,2,'2018-01-02 18:51:03','00NDUBDP',9455,'running',30),(31,0,'Shahad','San Fransisco','5345234','3-2_12-4_17-6',2,1,'2018-01-02 18:52:01','RJCVQS0N',9730,'running',31),(32,0,'sssss','sfgsdfgsf','45234','3-2_12-4_17-6_7-1',2,3,'2018-01-02 19:02:40','G92H8R3D',9835,'running',32),(33,0,'vcvzcvzc','rthfghd','41234','7-3',2,2,'2018-01-02 19:03:08','ONIKJ7EZ',344,'running',33),(34,0,'xvcbxv','gdfg','52345234','7-3_8-7',2,1,'2018-01-02 19:16:29','RZ5XBSO9',16736,'completed',34),(35,0,'zdfggsdfgs','sgdfgsdfgfd','retwertwertgws','7-3_8-7_11-5',2,2,'2018-01-02 19:04:02','PFLCZ61N',16848.1,'running',35);
/*!40000 ALTER TABLE `order_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `path`
--

DROP TABLE IF EXISTS `path`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `path` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `path`
--

LOCK TABLES `path` WRITE;
/*!40000 ALTER TABLE `path` DISABLE KEYS */;
/*!40000 ALTER TABLE `path` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `price` varchar(64) DEFAULT NULL,
  `discount` varchar(64) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `remaining_qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'uioyui','jhkl','102',NULL,4,NULL),(2,'neymar','next world cup champion','22',NULL,3,NULL),(3,'marcelo','353','43',NULL,4,4),(4,'neyyyyy','111','33',NULL,3,NULL),(5,'Al-Amin','www','2',NULL,4,NULL),(6,'couta','seiii','1424',NULL,9,NULL),(7,'coutinho','boss','113',NULL,5,96),(8,'M12','wqrqw','2341',NULL,6,NULL),(9,'PC','rqwe','12',NULL,13,NULL),(10,'njr','hiiii I am al-amin','23',NULL,9,NULL),(11,'p. coutinho','www','23.42',NULL,2,785),(12,'M12','wqrqw','2341',NULL,6,5),(13,'p. coutinho','www','22',NULL,2,10),(14,'neyyyyy','111','33',NULL,9,20),(15,'p. coutinho','www','23',NULL,2,7855),(16,'uioyui','jhkl','12.34',NULL,2,7),(17,'qqq','r','45',NULL,5,4),(18,'p. coutinho','www','2',NULL,9,78556),(19,'erqwerqe','rq','2.2',NULL,5,4),(20,'aaa','aaaaaa','1111',NULL,4,1111);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registered_mobiles`
--

DROP TABLE IF EXISTS `registered_mobiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registered_mobiles` (
  `imie_number` varchar(110) NOT NULL,
  `imie_number_2` varchar(110) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`imie_number`),
  UNIQUE KEY `registered_mobiles_imie_number_uindex` (`imie_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registered_mobiles`
--

LOCK TABLES `registered_mobiles` WRITE;
/*!40000 ALTER TABLE `registered_mobiles` DISABLE KEYS */;
INSERT INTO `registered_mobiles` VALUES ('0',NULL,NULL),('357558066933272','357558066933272',1);
/*!40000 ALTER TABLE `registered_mobiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email_uindex` (`email`),
  UNIQUE KEY `user_id_uindex` (`id`),
  UNIQUE KEY `user_username_uindex` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'alamin','alaminbbsc@gmail.com','$2a$12$XA8pTt7PvnAjgjjZHaX4QOGFmP2gGn4jSmANKjJ3zJal221iPa9ua','01711389230','Dhaka'),(2,'al','al@al','$2a$12$G.PZmEcvDD2kxdWkqMPgbuZqtg8YwBquKUpa2m2SngbeN1CegkHSi','1241234','trtwer546546'),(3,'al2','z@z11','$2a$12$suUjsJXxIJLtFCrcHjcCOeuLxSyK8.6pkEuTyTzmt141Bp0wRDbCW','11','111'),(4,'admin','admin@tajashop.com','$2a$12$oamffj0YzUCl7cIPonjVceB3Xg4ZQXSaE1Zbwj.wbzpHwL9KLpHmO','+88017111111','SUST, Sylhet'),(5,'ala','aa@aaa','$2a$12$.6f9UvvGTOGWUmtp1INSneK1mO.AwFt/kmXOuTJnVLGt3Ng52QA3S','3434','wertwer');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-03  1:24:46
