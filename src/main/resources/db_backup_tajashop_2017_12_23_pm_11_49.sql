-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: tajashop
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `category_number` int(11) DEFAULT NULL,
  `subcategory_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Fresh Vegitable',1,0),(2,'Fresh Fruits',2,0),(3,'Meat & Poultry',3,0),(4,'Chicken',3,1),(5,'Mutton',3,2),(6,'Beef',3,3),(7,'Seafood & Fish',4,0),(8,'Grains',5,0),(9,'Whole Grains',5,1),(10,'Others',5,2),(11,'Desert & Baked',6,0),(12,'Frozen Foods',7,0),(13,'Spices',8,0),(14,'Dry & Canned',9,0);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_`
--

DROP TABLE IF EXISTS `order_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `name` varchar(110) DEFAULT NULL,
  `address` varchar(1010) DEFAULT NULL,
  `mobile` varchar(110) DEFAULT NULL,
  `product_info` varchar(1010) DEFAULT NULL,
  `delivery_boy` int(11) DEFAULT NULL,
  `delivery_type` int(11) DEFAULT NULL,
  `order_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `payment_token` varchar(110) DEFAULT NULL,
  `grand_total` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_`
--

LOCK TABLES `order_` WRITE;
/*!40000 ALTER TABLE `order_` DISABLE KEYS */;
INSERT INTO `order_` VALUES (1,0,'ert','etrt','ert','3:1_5:1_7:2_1:6',1,2,'2017-12-23 17:08:39','B-77-79-1533',888),(2,0,'sdf','Dfasdfasd','fsd','3:1_5:1_7:2_1:6',1,3,'2017-12-23 17:09:54','2EAA-240D5',885);
/*!40000 ALTER TABLE `order_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `price` varchar(64) DEFAULT NULL,
  `discount` varchar(64) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `remaining_qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'uioyui','jhkl','102',NULL,4,NULL),(2,'neymar','next world cup champion','22',NULL,3,NULL),(3,'marcelo','353','43',NULL,4,4),(4,'neyyyyy','111','33',NULL,3,NULL),(5,'Al-Amin','www','2',NULL,4,NULL),(6,'couta','seiii','1424',NULL,9,NULL),(7,'coutinho','boss','113',NULL,5,96),(8,'M12','wqrqw','2341',NULL,6,NULL),(9,'PC','rqwe','12',NULL,13,NULL),(10,'njr','hiiii I am al-amin','23',NULL,9,NULL),(11,'p. coutinho','www','23.42',NULL,2,785),(12,'M12','wqrqw','2341',NULL,6,5),(13,'p. coutinho','www','22',NULL,2,10),(14,'neyyyyy','111','33',NULL,9,20),(15,'p. coutinho','www','23',NULL,2,7855),(16,'uioyui','jhkl','12.34',NULL,2,7),(17,'qqq','r','45',NULL,5,4),(18,'p. coutinho','www','2',NULL,9,78556),(19,'erqwerqe','rq','2.2',NULL,5,4);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email_uindex` (`email`),
  UNIQUE KEY `user_id_uindex` (`id`),
  UNIQUE KEY `user_username_uindex` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'alamin','alaminbbsc@gmail.com','$2a$12$XA8pTt7PvnAjgjjZHaX4QOGFmP2gGn4jSmANKjJ3zJal221iPa9ua','01711389230','Dhaka'),(2,'al','al@al','$2a$12$G.PZmEcvDD2kxdWkqMPgbuZqtg8YwBquKUpa2m2SngbeN1CegkHSi','1241234','trtwer546546'),(3,'al2','z@z11','$2a$12$suUjsJXxIJLtFCrcHjcCOeuLxSyK8.6pkEuTyTzmt141Bp0wRDbCW','11','111'),(4,'admin','admin@tajashop.com','$2a$12$oamffj0YzUCl7cIPonjVceB3Xg4ZQXSaE1Zbwj.wbzpHwL9KLpHmO','+88017111111','SUST, Sylhet');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-23 23:49:54
