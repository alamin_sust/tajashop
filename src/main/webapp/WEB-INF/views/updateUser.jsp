<%@page import="java.io.DataInputStream" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="com.tajashop.connection.Database" %>
<%@page import="java.io.OutputStream" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.File" %>
<%@page import="java.io.InputStream" %>
<%@ page import="com.sun.org.apache.regexp.internal.RE" %>
<%@ page import="static com.tajashop.web.util.EncryptionUtil.matchWithSecuredHash" %>
<%@ page import="static com.tajashop.web.util.EncryptionUtil.generateSecuredHash" %>


<%@ include file="header.jsp" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%
    if (session.getAttribute("username") == null || session.getAttribute("username").equals("")) {
        response.sendRedirect("home");
    }

    Database db = new Database();
    db.connect();
    //try{

    String username = "";
    String email = "";
    String address = "";
    String phone = "";
    String password = "";


    if ((request.getParameter("updateUser") != null && !request.getParameter("updateUser").equals(""))) {

        String q3 = "select * from user where id=" + session.getAttribute("userId").toString();
        Statement st3 = db.connection.createStatement();
        ResultSet rs3 = st3.executeQuery(q3);
        rs3.next();

        password = rs3.getString("password");

        boolean canUpdate = false;
        String passwordToInsert = "";

        if (request.getParameter("password") != null && matchWithSecuredHash(request.getParameter("password"), password)) {
            if ((request.getParameter("newPassword") != null && !request.getParameter("newPassword").equals(""))
                    || (request.getParameter("newPassword2") != null && !request.getParameter("newPassword2").equals(""))) {
                if (request.getParameter("newPassword") != null && request.getParameter("newPassword2") != null
                        && request.getParameter("newPassword").equals(request.getParameter("newPassword2"))) {
                    canUpdate = true;
                    passwordToInsert = request.getParameter("newPassword");

                } else {
                    session.setAttribute("errorMsg", "New Passwords Doesn't Match!");
                }
            } else {
                canUpdate = true;
                passwordToInsert = request.getParameter("password");
            }
        } else {
            session.setAttribute("errorMsg", "Password Incorrect!");
        }


        String q4 = "update user set email='" + request.getParameter("email") + "', address='" + request.getParameter("address")
                + "',phone='" + request.getParameter("phone") + "', password='" + generateSecuredHash(passwordToInsert)
                + "' where id=" + session.getAttribute("userId");
        Statement st4 = db.connection.createStatement();

        if(canUpdate) {
            st4.executeUpdate(q4);
            session.setAttribute("successMsg", "Successfully Updated!");
        }

    }
    String q5 = "select * from user where id=" + session.getAttribute("userId").toString();
    Statement st5 = db.connection.createStatement();
    ResultSet rs5 = st5.executeQuery(q5);
    rs5.next();
    username = session.getAttribute("username").toString();
    email = rs5.getString("email");
    address = rs5.getString("address");
    phone = rs5.getString("phone");

%>


<div class="w3l_banner_nav_right">

    <div class="col col-sm-10 col-sm-offset-1">
        <%if (session.getAttribute("successMsg") != null && !session.getAttribute("successMsg").equals("")) {%>
        <div class="alert alert-success"><%=session.getAttribute("successMsg")%>
        </div>
        <%}%>
        <%
            session.setAttribute("successMsg", null);
        %>
        <%if (session.getAttribute("errorMsg") != null && !session.getAttribute("errorMsg").equals("")) {%>
        <div class="alert alert-danger"><%=session.getAttribute("errorMsg")%>
        </div>
        <%}%>
        <%
            session.setAttribute("errorMsg", null);
        %>


        <form method="POST" action="updateUser">
            <tr>
                <td>Username</td>
                <td><input name="username" class="form-control" type="text" value="<%=username%>" disabled/></td>
            </tr>
            <br>
            <tr>
                <td>Email</td>
                <td><input name="email" type="email" id="" value="<%=email%>" class="form-control" required/>
                </td>
            </tr>
            <br>
            <tr>
                <td>Address</td>
                <td><input name="address" type="text" class="aksTA form-control" value="<%=address%>" maxlength="500"
                           required></td>
            </tr>
            <br>
            <tr>
                <td>Phone</td>
                <td><input name="phone" type="text" class="aksTA form-control" value="<%=phone%>"
                           maxlength="500" required></td>
            </tr>
            <br>
            <tr>
                <td>Password</td>
                <td><input name="password" type="password" class="aksTA form-control" value=""
                           maxlength="500" required></td>
            </tr>
            <br>
            <tr>
                <td>New Password</td>
                <td><input name="newPassword" type="password" class="aksTA form-control" value=""
                           maxlength="500"></td>
            </tr>
            <br>
            <tr>
                <td>Confirm New Password</td>
                <td><input name="newPassword2" type="password" class="aksTA form-control" value=""
                           maxlength="500"></td>
            </tr>
            <br>
            <input type="hidden" name="updateUser" value="true">
            <button type="submit" id="btnSaveChanges" class="btn btn-success">Update<span class="fa fa-save"></span>
            </button>
        </form>
    </div>

</div>

<div class="clearfix"></div>
</div>
<!-- //banner -->
<!-- newsletter-top-serv-btm -->
<div class="newsletter-top-serv-btm">
    <div class="container">
        <div class="col-md-4 wthree_news_top_serv_btm_grid">
            <div class="wthree_news_top_serv_btm_grid_icon">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            </div>
            <h3>Nam libero tempore</h3>
            <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus
                saepe eveniet ut et voluptates repudiandae sint et.</p>
        </div>
        <div class="col-md-4 wthree_news_top_serv_btm_grid">
            <div class="wthree_news_top_serv_btm_grid_icon">
                <i class="fa fa-bar-chart" aria-hidden="true"></i>
            </div>
            <h3>officiis debitis aut rerum</h3>
            <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus
                saepe eveniet ut et voluptates repudiandae sint et.</p>
        </div>
        <div class="col-md-4 wthree_news_top_serv_btm_grid">
            <div class="wthree_news_top_serv_btm_grid_icon">
                <i class="fa fa-truck" aria-hidden="true"></i>
            </div>
            <h3>eveniet ut et voluptates</h3>
            <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus
                saepe eveniet ut et voluptates repudiandae sint et.</p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<%
    /*}catch (Exception e) {

    } finally {
            db.close();
    }*/
%>
<!-- //newsletter-top-serv-btm -->
<%@ include file="footer.jsp" %>














