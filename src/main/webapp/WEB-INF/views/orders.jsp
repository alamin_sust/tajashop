<%@ taglib prefix="b" uri="http://www.springframework.org/tags/form" %>
<%--
    Document   : currentLocation
    Created on : Feb 25, 2017, 2:17:08 AM
    Author     : Al-Amin
--%>

<%@page import="com.tajashop.connection.Database" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.ArrayList" %>
<%@ page import="com.tajashop.web.util.Constants" %>
<%@ page import="java.sql.Struct" %>
<%@ page import="java.util.Arrays" %>

<%@ include file="header.jsp" %>

<%
    if (session.getAttribute("username")==null || session.getAttribute("username").equals("")) {
        response.sendRedirect("home");
    }

    Database db = new Database();
    db.connect();
    // try {

    Statement st = db.connection.createStatement();
    String q="select * from order_ WHERE customer_id = (SELECT id from user where username='"
            +session.getAttribute("username")+"')";
    ResultSet rs = st.executeQuery(q);
%>
<div class="col-sm-9">
<%
while (rs.next()) {
%>
<div class="well">
    <div class="row">
<div class="col col-sm-9">
    <label>Order Details:</label>
    <ul>
        <li>Order Time : <%=rs.getString("order_time")%></li>
    <%
        List<String> products = Arrays.asList(rs.getString("product_info").split("_"));

        for(String product:products) {
        Statement st2 = db.connection.createStatement();
        String productId = product.split("-")[0];
        String productQty = product.split("-")[1];
        String q2="select * from product WHERE id = "+productId;
        ResultSet rs2 = st2.executeQuery(q2);
        rs2.next();
    %>
        <li>
            <%=rs2.getString("name")%> : <%=productQty%> unit(s).
        </li>
    <%}%>
    </ul>

</div>
 <div class="col col-sm-3">
     <form method="post" action="orderStatus">
         <%if(rs.getString("status").equals("running")){%>
         <input type="hidden" name="pathId" value="<%=rs.getString("path_id")%>">
         <button class="btn btn-default"  type="submit">Track Delivery</button>
         <%}else{%>
         <button class="btn btn-default" disabled>Order Already Delivered</button>
         <%}%>
     </form>
 </div>
</div>
</div>
<%
    }
    %>
</div>
<div class="row"></div>

        <%
    /*}catch (Exception e){
        response.sendRedirect("somethingWentWrong");
    }finally {
        db.close();
    }*/


%>
<%@ include file="footer.jsp" %>
