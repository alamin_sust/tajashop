<%@ taglib prefix="b" uri="http://www.springframework.org/tags/form" %>
<%--
    Document   : currentLocation
    Created on : Feb 25, 2017, 2:17:08 AM
    Author     : Al-Amin
--%>

<%@page import="com.tajashop.connection.Database" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.ArrayList" %>
<%@ page import="com.tajashop.web.util.Constants" %>
<%@ page import="java.sql.Struct" %>

<%@ include file="header.jsp" %>
<div id="fulpage">
    <style>
        #map {
            position: relative;

            overflow: auto;
        }
    </style>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"
            type="text/javascript"></script>

    <%
        Database db = new Database();
        db.connect();
        // try {

        String pathId = request.getParameter("pathId");
        Statement st = db.connection.createStatement();
        String q="select * from delivery_boy_location where path_id="+pathId+" order by id DESC ";
        ResultSet rs = st.executeQuery(q);

        if(!rs.next()) {
            session.setAttribute("errorMsg", "No Order History Found!");
            response.sendRedirect("home");
        }

        String datetime;
        String longitude;
        String latitude;

        datetime = rs.getString("datetime");
        longitude = rs.getString("longitude");
        latitude = rs.getString("latitude");




    %>
    <div class="container">
        <div class="row">
            <%--<div><button style="float: right;margin-right: 100px;" class="btn btn-primary" value="Reload Page" onClick="history.go(0)">Refresh Map</button></div>
            <div id="map" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></div>--%>
            <div><button class="btn btn-primary" value="Reload Page" onClick="history.go(0)">Refresh Map</button></div>
            <div id="map"  style="height: 1024px;"></div>
        </div>
    </div>

    <script type="text/javascript">
        var datetime = <%=datetime%>;
        var longitude = <%=longitude%>;
        var latitude = <%=latitude%>;
        var pathId = <%=pathId%>;


        var centerPointX = latitude;
        var centerPointY = longitude;
        var zoom = 13;





        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: zoom,
            center: new google.maps.LatLng(centerPointX, centerPointY),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        var mk = ['blue-dot', 'red-dot', 'yellow-dot', 'green-dot'];



        (function(){

            var status = "n/a";
            var image = "n/a";
            var label = "n/a";
            var markerPoint = "";

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(centerPointX, centerPointY),
                animation: google.maps.Animation.DROP,
                map: map,
                label: "Delivery Boy Currently Here"
            });



            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {

                    infowindow.setContent("Time: " + datetime + "<br>Latitude: " + latitude + "<br>Longitude: " + longitude + "<br>Path ID: " + pathId + "</b>");

                    infowindow.open(map, marker);

                }
            })(marker, i));

        })();


    </script>



</div>
<%
    /*}catch (Exception e){
        response.sendRedirect("somethingWentWrong");
    }finally {
        db.close();
    }*/


%>
<%@ include file="footer.jsp" %>


