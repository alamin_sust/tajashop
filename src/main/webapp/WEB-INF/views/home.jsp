<%@ page import="java.util.Random" %>
<%@ include file="header.jsp" %>

    <div class="w3l_banner_nav_right">
        <section class="slider">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="w3l_banner_nav_right_banner">
                            <h3>Make your <span>food</span> with Spicy.</h3>
                            <div class="more">
                                <a href="products" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="w3l_banner_nav_right_banner1">
                            <h3>Make your <span>food</span> with Spicy.</h3>
                            <div class="more">
                                <a href="products" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="w3l_banner_nav_right_banner2">
                            <h3>upto <i>50%</i> off.</h3>
                            <div class="more">
                                <a href="products" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <!-- flexSlider -->
        <link rel="stylesheet" href="resources/css/flexslider.css" type="text/css" media="screen" property="" />
        <script defer src="resources/js/jquery.flexslider.js"></script>
        <script type="text/javascript">
            $(window).load(function(){
                $('.flexslider').flexslider({
                    animation: "slide",
                    start: function(slider){
                        $('body').removeClass('loading');
                    }
                });
            });
        </script>
        <!-- //flexSlider -->
    </div>
    <div class="clearfix"></div>
</div>
<!-- banner -->
<div class="banner_bottom">
    <div class="wthree_banner_bottom_left_grid_sub">
    </div>
    <div class="wthree_banner_bottom_left_grid_sub1">
        <div class="col-md-4 wthree_banner_bottom_left">
            <div class="wthree_banner_bottom_left_grid">
                <img src="resources/images/4.jpg" alt=" " class="img-responsive" />
                <div class="wthree_banner_bottom_left_grid_pos">
                    <h4>Discount Offer <span>25%</span></h4>
                </div>
            </div>
        </div>
        <div class="col-md-4 wthree_banner_bottom_left">
            <div class="wthree_banner_bottom_left_grid">
                <img src="resources/images/5.jpg" alt=" " class="img-responsive" />
                <div class="wthree_banner_btm_pos">
                    <h3>introducing <span>best store</span> for <i>groceries</i></h3>
                </div>
            </div>
        </div>
        <div class="col-md-4 wthree_banner_bottom_left">
            <div class="wthree_banner_bottom_left_grid">
                <img src="resources/images/6.jpg" alt=" " class="img-responsive" />
                <div class="wthree_banner_btm_pos1">
                    <h3>Save <span>Upto</span> $10</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
</div>

<%

    Database db = new Database();
    db.connect();

    try {

        String q2 = "select * from product p join category c on(p.category_id = c.id) where c.id>0 and c.name is not null and c.category_number=3 and c.subcategory_number!=0";


        Statement st2  = db.connection.createStatement();
        ResultSet rs2 = st2.executeQuery(q2);

        int cnt2=0;

%>



<!-- halal-food -->
<div class="top-brands">
    <div class="container">
        <h3>Certified Halal Meat & Poultry</h3>
        <div class="agile_top_brands_grids">
            <%while (rs2.next()&&cnt2++<8) {%>
            <div class="col-md-3 top_brand_left">
                <div class="hover14 column">
                    <div class="agile_top_brand_left_grid">
                        <div class="tag"><a href="single?id=<%=rs2.getString("id")%>"><img src="resources/images/products/<%=rs2.getString("id")%>.jpg" alt=" " class="img-responsive"/></a></div>
                        <div class="agile_top_brand_left_grid1">
                            <figure>
                                <div class="snipcart-item block">
                                    <div class="snipcart-thumb">
                                        <br><br><br><br><br><br><br>
                                        <p><%=rs2.getString("name")%></p>
                                        <h4>$<%=rs2.getString("price")%> <%--<span>$10.00</span>--%></h4>
                                    </div>
                                    <div class="snipcart-details top_brand_home_details">
                                        <%if(session.getAttribute("username")!=null
                                                && session.getAttribute("username").equals("admin")) {%>
                                        <form action="addProduct" method="get">
                                            <input type="hidden" name="updateProduct" value="<%=rs2.getString("id")%>">
                                            <input type="submit" name="submit" value="Update Product" class="button"/>
                                        </form>
                                        <%}else{%>
                                        <form action="checkout" method="post">
                                            <fieldset>
                                                <input type="hidden" name="item_id" value="<%=rs2.getString("id")%>"/>
                                                <input type="hidden" name="cmd" value="_cart"/>
                                                <input type="hidden" name="add" value="1"/>
                                                <input type="hidden" name="business" value=" "/>
                                                <input type="hidden" name="item_name" value="<%=rs2.getString("name")%>"/>
                                                <input type="hidden" name="amount" value="<%=rs2.getString("price")%>"/>
                                                <input type="hidden" name="discount_amount" value="0.00"/>
                                                <input type="hidden" name="currency_code" value="USD"/>
                                                <input type="hidden" name="return" value=" "/>
                                                <input type="hidden" name="cancel_return" value=" "/>
                                                <input type="submit" name="submit" value="Add to cart" class="button"/>
                                                <br>
                                                <a class="btn btn-info" href="single?id=<%=rs2.getString("id")%>">Product Details</a>
                                            </fieldset>
                                        </form>
                                        <%}%>


                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <%}%>

            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //halal food-->
<!-- top-brands -->
<div class="top-brands">
    <div class="container">
        <h3>Hot Offers</h3>
        <div class="agile_top_brands_grids">
            <%

                Random rand  = new Random();




                String q3 = "select * from product WHERE id>0 and id>="+((rand.nextInt()%8)+1);


                Statement st3  = db.connection.createStatement();
                ResultSet rs3 = st3.executeQuery(q3);
                int cnt3=0;

                while (rs3.next() && cnt3++<4) {%>
            <div class="col-md-3 top_brand_left">
                <div class="hover14 column">
                    <div class="agile_top_brand_left_grid">
                        <div class="tag"><a href="single?id=<%=rs3.getString("id")%>"><img src="resources/images/products/<%=rs3.getString("id")%>.jpg" alt=" " class="img-responsive"/></a></div>
                        <div class="agile_top_brand_left_grid1">
                            <figure>
                                <div class="snipcart-item block">
                                    <div class="snipcart-thumb">
                                        <br><br><br><br><br><br><br>
                                        <p><%=rs3.getString("name")%></p>
                                        <h4>$<%=rs3.getString("price")%> <%--<span>$10.00</span>--%></h4>
                                    </div>
                                    <div class="snipcart-details top_brand_home_details">
                                        <%if(session.getAttribute("username")!=null
                                                && session.getAttribute("username").equals("admin")) {%>
                                        <form action="addProduct" method="get">
                                            <input type="hidden" name="updateProduct" value="<%=rs3.getString("id")%>">
                                            <input type="submit" name="submit" value="Update Product" class="button"/>
                                        </form>
                                        <%}else{%>
                                        <form action="checkout" method="post">
                                            <fieldset>
                                                <input type="hidden" name="item_id" value="<%=rs3.getString("id")%>"/>
                                                <input type="hidden" name="cmd" value="_cart"/>
                                                <input type="hidden" name="add" value="1"/>
                                                <input type="hidden" name="business" value=" "/>
                                                <input type="hidden" name="item_name" value="<%=rs3.getString("name")%>"/>
                                                <input type="hidden" name="amount" value="<%=rs3.getString("price")%>"/>
                                                <input type="hidden" name="discount_amount" value="0.00"/>
                                                <input type="hidden" name="currency_code" value="USD"/>
                                                <input type="hidden" name="return" value=" "/>
                                                <input type="hidden" name="cancel_return" value=" "/>
                                                <input type="submit" name="submit" value="Add to cart" class="button"/>
                                                <br>
                                                <a class="btn btn-info" href="single?id=<%=rs3.getString("id")%>">Product Details</a>
                                            </fieldset>
                                        </form>
                                        <%}%>


                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <%}%>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //top-brands -->
<!-- fresh-vegetables -->
<div class="fresh-vegetables">
    <div class="container">
        <h3>Top Products</h3>
        <div class="w3l_fresh_vegetables_grids">
            <%--<div class="col-md-3 w3l_fresh_vegetables_grid w3l_fresh_vegetables_grid_left">
                <div class="w3l_fresh_vegetables_grid2">
                    <ul>
                        <li><i class="fa fa-check" aria-hidden="true"></i><a href="products">All Brands</a></li>
                        <li><i class="fa fa-check" aria-hidden="true"></i><a href="vegetables.html">Vegetables</a></li>
                        <li><i class="fa fa-check" aria-hidden="true"></i><a href="vegetables.html">Fruits</a></li>
                        <li><i class="fa fa-check" aria-hidden="true"></i><a href="drinks.html">Juices</a></li>
                        <li><i class="fa fa-check" aria-hidden="true"></i><a href="pet.html">Pet Food</a></li>
                        <li><i class="fa fa-check" aria-hidden="true"></i><a href="bread.html">Bread & Bakery</a></li>
                        <li><i class="fa fa-check" aria-hidden="true"></i><a href="household.html">Cleaning</a></li>
                        <li><i class="fa fa-check" aria-hidden="true"></i><a href="products">Spices</a></li>
                        <li><i class="fa fa-check" aria-hidden="true"></i><a href="products">Dry Fruits</a></li>
                        <li><i class="fa fa-check" aria-hidden="true"></i><a href="products">Dairy Products</a></li>
                    </ul>
                </div>
            </div>--%>
            <div class="col-md-12 w3l_fresh_vegetables_grid_right">
                <div class="col-md-4 w3l_fresh_vegetables_grid">
                    <div class="w3l_fresh_vegetables_grid1">
                        <img src="resources/images/8.jpg" alt=" " class="img-responsive" />
                    </div>
                </div>
                <div class="col-md-4 w3l_fresh_vegetables_grid">
                    <div class="w3l_fresh_vegetables_grid1">
                        <div class="w3l_fresh_vegetables_grid1_rel">
                            <img src="resources/images/7.jpg" alt=" " class="img-responsive" />
                            <div class="w3l_fresh_vegetables_grid1_rel_pos">
                                <div class="more m1">
                                    <a href="products" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w3l_fresh_vegetables_grid1_bottom">
                        <img src="resources/images/10.jpg" alt=" " class="img-responsive" />
                        <div class="w3l_fresh_vegetables_grid1_bottom_pos">
                            <h5>Special Offers</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 w3l_fresh_vegetables_grid">
                    <div class="w3l_fresh_vegetables_grid1">
                        <img src="resources/images/9.jpg" alt=" " class="img-responsive" />
                    </div>
                    <div class="w3l_fresh_vegetables_grid1_bottom">
                        <img src="resources/images/11.jpg" alt=" " class="img-responsive" />
                    </div>
                </div>
                <div class="clearfix"> </div>
                <div class="agileinfo_move_text">
                    <div class="agileinfo_marquee">
                        <h4>get <span class="blink_me">25% off</span> on first order and also get gift voucher</h4>
                    </div>
                    <div class="agileinfo_breaking_news">
                        <span> </span>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //fresh-vegetables -->

<%--<!-- newsletter -->
<div class="newsletter">
    <div class="container">
        <div class="w3agile_newsletter_left">
            <h3>sign up for our newsletter</h3>
        </div>
        <div class="w3agile_newsletter_right">
            <form action="#" method="post">
                <input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                <input type="submit" value="subscribe now">
            </form>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!-- //newsletter -->--%>

<%
    } catch (Exception e) {
        e.printStackTrace();
    } finally {
        db.close();
    }
%>
<%@ include file="footer.jsp" %>