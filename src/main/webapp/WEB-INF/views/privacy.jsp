<%@page import="java.io.DataInputStream" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="com.tajashop.connection.Database" %>
<%@page import="java.io.OutputStream" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.File" %>
<%@page import="java.io.InputStream" %>
<%@ page import="com.sun.org.apache.regexp.internal.RE" %>
<%@ page import="static com.tajashop.web.util.EncryptionUtil.matchWithSecuredHash" %>
<%@ page import="static com.tajashop.web.util.EncryptionUtil.generateSecuredHash" %>


<%@ include file="header.jsp" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div class="w3l_banner_nav_right">
    <!-- privacy -->
    <div class="privacy">
        <div class="privacy1">
            <h3>Privacy Policy</h3>
            <div class="banner-bottom-grid1 privacy1-grid">
                <ul>
                    <li><i class="glyphicon glyphicon-user" aria-hidden="true"></i></li>
                    <li>Profile <span>Excepteur sint occaecat cupidatat non proident, sunt in
							culpa qui officia deserunt mollit anim id est laborum.</span></li>
                </ul>
                <ul>
                    <li><i class="glyphicon glyphicon-search" aria-hidden="true"></i></li>
                    <li>Search <span>Excepteur sint occaecat cupidatat non proident, sunt in
							culpa qui officia deserunt mollit anim id est laborum.</span></li>
                </ul>
                <ul>
                    <li><i class="glyphicon glyphicon-paste" aria-hidden="true"></i></li>
                    <li>News Feed <span>Excepteur sint occaecat cupidatat non proident, sunt in
							culpa qui officia deserunt mollit anim id est laborum.</span></li>
                </ul>
                <ul>
                    <li><i class="glyphicon glyphicon-qrcode" aria-hidden="true"></i></li>
                    <li>Applications <span>Excepteur sint occaecat cupidatat non proident, sunt in
							culpa qui officia deserunt mollit anim id est laborum.</span></li>
                </ul>
            </div>
        </div>
        <div class="privacy1">
            <h3>Terms of Use</h3>
            <div class="banner-bottom-grid1 privacy2-grid">
                <div class="privacy2-grid1">
                    <h4>deserunt mollit anim id est laborum?</h4>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    <div class="privacy2-grid1-sub">
                        <h5>1. sint occaecat cupidatat non proident, sunt</h5>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in
                            culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <div class="privacy2-grid1-sub">
                        <h5>2.perspiciatis unde omnis iste natus error</h5>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in
                            culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <div class="privacy2-grid1-sub">
                        <h5>3. natus error sit voluptatem accusant</h5>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in
                            culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <div class="privacy2-grid1-sub">
                        <h5>4. occaecat cupidatat non proident, sunt in</h5>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in
                            culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <div class="privacy2-grid1-sub">
                        <h5>5. deserunt mollit anim id est laborum</h5>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in
                            culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //privacy -->
</div>
<div class="clearfix"></div>
</div>
<%@ include file="footer.jsp" %>