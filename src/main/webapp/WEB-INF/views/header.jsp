<%@ page import="java.sql.Statement" %>
<%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 08-Dec-17
  Time: 4:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="com.tajashop.connection.Database" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
    <title>Taja Shop, LLC.</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Grocery Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);
    function hideURLbar() {
        window.scrollTo(0, 1);
    } </script>
    <!-- //for-mobile-apps -->
    <link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="resources/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- font-awesome icons -->
    <link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="resources/js/jquery-1.11.1.min.js"></script>
    <!-- //js -->
    <link href='//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic'
          rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
          rel='stylesheet' type='text/css'>
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="resources/js/move-top.js"></script>
    <script type="text/javascript" src="resources/js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>
    <!-- start-smoth-scrolling -->

    <!-- search suggestion-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <%
    Database dbH = new Database();
    dbH.connect();
    try {

        Statement stH0 = dbH.connection.createStatement();
        String qH0= "select * from product where id>0";
        ResultSet rsH0 = stH0.executeQuery(qH0);

        String nameList = "[";

        int cntH=0;
        while (rsH0.next()) {
            if(cntH!=0) {
                nameList+=",";
            }
            nameList+="\""+rsH0.getString("name")+"\"";
            cntH++;
        }
        nameList+="]";

    %>


    <script>
        $( function() {
            var availableTags = <%=nameList%>;
            $( "#search" ).autocomplete({
                source: availableTags
            });
        } );
    </script>
    <!--end suggestion-->
    <%

    %>
</head>

<body>

<!-- header -->
<div class="agileits_header">
    <div class="w3l_offers">
        <a href="products?category=offers">Today's special Offers !</a>
    </div>
    <div class="w3l_search">
        <form action="products" method="post">
            <input id="search" type="text" name="searchProduct" value="Search a product..." onfocus="this.value = '';"
                   onblur="if (this.value == '') {this.value = 'Search a product...';}" required="">
            <input type="submit" value=" ">
        </form>
    </div>
    <%if(!request.getAttribute("javax.servlet.forward.request_uri").toString().endsWith("checkout")) {%>
    <div class="product_list_header">
        <form action="#" method="post" class="last">
            <fieldset>
                <input type="hidden" name="cmd" value="_cart"/>
                <input type="hidden" name="display" value="1"/>
                <input type="submit" name="submit" value="View your cart" class="button"/>
            </fieldset>
        </form>
    </div>
    <%}%>
    <div class="w3l_header_right">
        <ul>



                        <%if(session.getAttribute("userId")!=null && !session.getAttribute("userId").equals("")){%>
            <div style="color: white"><span class="glyphicon glyphicon-user"></span> logged in as <%=session.getAttribute("username")%> <a href="updateUser">update info</a><form method="post" action="login">
                <input type="hidden" name="logout" value="true">
                <button class="btn btn-link" type="submit" value="Logout">Logout</button>
            </form>
                <form method="get" action="orders">
                    <button class="btn btn-link" type="submit">My Orders</button>
                </form></div>





                <%} else {%>
            <li class="dropdown profile_details_drop">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i><span
                        class="caret"></span></a>
                <div class="mega-dropdown-menu">
            <div class="w3ls_vegetables">
                <ul class="dropdown-menu drp-mnu">
                    <li><a href="login">Login</a></li>
                    <li><a href="login">Sign Up</a></li>
                </ul>
            </div>
                </div>
            </li>
                <%}%>



    </ul>
</div>
<%--<div class="w3l_header_right1">
    <h2><a href="contactUs">Contact Us</a></h2>
</div>--%>
<div class="clearfix"></div>
</div>
<!-- script-for sticky-nav -->
<script>
    $(document).ready(function () {
        var navoffeset = $(".agileits_header").offset().top;
        $(window).scroll(function () {
            var scrollpos = $(window).scrollTop();
            if (scrollpos >= navoffeset) {
                $(".agileits_header").addClass("fixed");
            } else {
                $(".agileits_header").removeClass("fixed");
            }
        });

    });
</script>
<!-- //script-for sticky-nav -->
<div class="logo_products">
    <div class="container">
        <div class="w3ls_logo_products_left">
            <h1><a href="home"><img src="resources/images/taja.png" alt="taja-logo"></a></h1>
        </div>
        <div class="w3ls_logo_products_left1">
            <ul class="special_items">
                <%--<li><a href="coupons">Coupons</a><i>/</i></li>
                <li><a href="specials">Specials</a><i>/</i></li>
                <li><a href="recepies">Recepies</a><i>/</i></li>
                <li><a href="holiday">Holiday</a><i>/</i></li>
                <li><a href="myRewards">My Rewards</a></li>--%>
                <%if(session.getAttribute("username")!=null && session.getAttribute("username").equals("admin")){%>
                <li><i>/</i><a href="addProduct">Add Product</a></li>
                <%}%>
            </ul>
        </div>
        <div class="w3ls_logo_products_left1">
            <ul class="phone_email">
                <li><i class="fa fa-phone" aria-hidden="true"></i>(+0123) 234 567</li>
                <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:store@grocery.com">store@grocery.com</a>
                </li>
            </ul>
        </div>


        <div class="clearfix"></div>
    </div>
</div>
<br>
<!-- //header -->
<%--<!-- products-breadcrumb -->
<div class="products-breadcrumb">
    <div class="container">
        <ul>
            <li><i class="fa fa-home" aria-hidden="true"></i><a href="index.html">Home</a><span>|</span></li>
            <li>Sign In & Sign Up</li>
        </ul>
    </div>
</div>
<!-- //products-breadcrumb -->--%>
    <%if(session.getAttribute("successMsg")!=null && !session.getAttribute("successMsg").equals("")){%>
<div class="alert alert-success"><%=session.getAttribute("successMsg")%>
</div>
    <%}%>
    <%if(session.getAttribute("errorMsg")!=null && !session.getAttribute("errorMsg").equals("")){%>
<div class="alert alert-danger"><%=session.getAttribute("errorMsg")%>
</div>
    <%}%>
    <%if(session.getAttribute("signupMsg")!=null && !session.getAttribute("signupMsg").equals("")){%>
<div class="alert alert-success"><%=session.getAttribute("signupMsg")%>
</div>
    <%}%>
    <%if(session.getAttribute("loginMsg")!=null && !session.getAttribute("loginMsg").equals("")){%>
<div class="alert alert-success"><%=session.getAttribute("loginMsg")%>
</div>
    <%}%>

    <%
            session.setAttribute("successMsg",null);
            session.setAttribute("signupMsg",null);
            session.setAttribute("loginMsg",null);
            session.setAttribute("errorMsg",null);
        %>
<!-- banner -->
<div class="banner">
    <div class="w3l_banner_nav_left">
        <nav class="navbar nav_bottom">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header nav_2">
                <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse"
                        data-target="#bs-megadropdown-tabs">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                <ul class="nav navbar-nav nav_1">
                    <li><a href="products">All Products</a></li>
                    <%

                            String qH = "select * from category where subcategory_number=0";
                            Statement stH = dbH.connection.createStatement();
                            ResultSet rsH = stH.executeQuery(qH);

                            while (rsH.next()) {
                                int category = rsH.getInt("category_number");

                                String name = rsH.getString("name");


                        String qH2 = "select * from category where category_number=" + category + " and subcategory_number!=0";
                        Statement stH2 = dbH.connection.createStatement();
                        ResultSet rsH2 = stH2.executeQuery(qH2);
                        int cnt = 0;

                        if(!rsH2.next()){
                    %>
                    <li><a href="products?category=<%=category%>"><%=name%>
                    </a></li>
                    <%} else {
                            rsH2.beforeFirst();
                    }
                        while (rsH2.next()) {
                            int subCategory = rsH2.getInt("subcategory_number");
                            String subName = rsH2.getString("name");
                            if (cnt == 0) {
                    %>
                    <li class="dropdown mega-dropdown active">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><%=name%><span
                                class="caret"></span></a>
                        <div class="dropdown-menu mega-dropdown-menu w3ls_vegetables_menu">
                            <div class="w3ls_vegetables">
                                <ul>
                                    <%}%>
                                    <li>
                                        <a href="products?category=<%=category%>&subCategory=<%=subCategory%>"><%=subName%>
                                        </a></li>
                                    <%
                                            cnt++;
                                        }
                                        if (cnt > 0) {
                                    %>

                                </ul>
                            </div>
                        </div>
                    </li>
                    <%
                                }
                            }
                        } catch (Exception e) {

                        } finally {
                            dbH.close();
                        }
                    %>

                    <%--<li><a href="products?category=1">Fresh Vegitable</a></li>
                    <li><a href="products?category=2">Fresh Fruits</a></li>
                    <li><a href="products?category=3">Meat & Poultry</a></li>
                    <li class="dropdown mega-dropdown active">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Veggies & Fruits<span class="caret"></span></a>
                        <div class="dropdown-menu mega-dropdown-menu w3ls_vegetables_menu">
                            <div class="w3ls_vegetables">
                                <ul>
                                    <li><a href="products?category=3&subCategory=1">Chicken</a></li>
                                    <li><a href="products?category=3&subCategory=2">Mutton</a></li>
                                    <li><a href="products?category=3&subCategory=3">Beef</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li><a href="products?category=4">Seafood & Fish</a></li>
                    <li><a href="products?category=5">Grains</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Beverages<span class="caret"></span></a>
                        <div class="dropdown-menu mega-dropdown-menu w3ls_vegetables_menu">
                            <div class="w3ls_vegetables">
                                <ul>
                                    <li><a href="products?category=5&subCategory=1">Whole Grains</a></li>
                                    <li><a href="products?category=5&subCategory=2">Others</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li><a href="products?category=6">Desert & Baked</a></li>
                    <li><a href="products?category=7">Frozen Foods</a></li>
                    <li><a href="products?category=8">Spices</a></li>
                    <li><a href="products?category=9">Dry & Canned</a></li>--%>

                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </div>
