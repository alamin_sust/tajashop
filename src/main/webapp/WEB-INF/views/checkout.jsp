<%@page import="java.io.DataInputStream" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="com.tajashop.connection.Database" %>
<%@page import="java.io.OutputStream" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.File" %>
<%@page import="java.io.InputStream" %>
<%@ page import="com.sun.org.apache.regexp.internal.RE" %>
<%@ page import="static com.tajashop.web.util.EncryptionUtil.matchWithSecuredHash" %>
<%@ page import="static com.tajashop.web.util.EncryptionUtil.generateSecuredHash" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.tajashop.web.util.DeliveryUtil" %>
<%@ page import="com.tajashop.web.util.FCMMessageSender" %>


<%@ include file="header.jsp" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

    <div class="w3l_banner_nav_right">

        <%
            /*if(session.getAttribute("userId")==null || session.getAttribute("userId").equals("")) {
                response.sendRedirect("home");
            }*/

            Database db = new Database();
            db.connect();

            try {

            if(request.getParameter("subTotal")!=null && !request.getParameter("subTotal").equals("")) {

                int productCnt = 0;
                String productInfo = "";
                while (request.getParameter("productId_"+(++productCnt)) != null && !request.getParameter("productId_"+(productCnt)).equals("")) {
                    if(productCnt!=1) {
                        productInfo+="_";
                    }
                    productInfo+=request.getParameter("productId_"+(productCnt))+"-"+request.getParameter("productQuantity_"+(productCnt));
                }
                int deliveryType;
                double deliveryCharge = 0;

                if(request.getParameter("deliveryType1")!=null && !request.getParameter("deliveryType1").equals("")) {
                    deliveryType=1;
                    deliveryCharge = 10;
                } else if(request.getParameter("deliveryType2")!=null && !request.getParameter("deliveryType2").equals("")) {
                    deliveryType=2;
                    deliveryCharge=5;
                } else {
                    deliveryType=3;
                    deliveryCharge=2;
                }

                String deliveryAddress = request.getParameter("address");

                int deliveryBoy = DeliveryUtil.getDeliveryBoyId(deliveryType,deliveryAddress);
                String mobile = request.getParameter("mobile");
                String paymentToken = request.getParameter("paymentToken");
                String customerId= session.getAttribute("userId")!=null?session.getAttribute("userId").toString():"0";
                String name= request.getParameter("name");
                double grandTotal= Double.valueOf(request.getParameter("subTotal"))+deliveryCharge;

                String qMx = "select max(id+1) as mxid from order_";
                Statement stMx = db.connection.createStatement();
                ResultSet rsMx = stMx.executeQuery(qMx);
                rsMx.next();
                String mxId = rsMx.getString("mxid");

                String q = "insert into order_ (id,customer_id,name,address,mobile,product_info,delivery_boy_id,delivery_type,order_time,payment_token,grand_total,path_id)" +
                        " values ("+mxId+","+customerId+",'"+name+"','"+deliveryAddress+"','"+mobile+"','"+productInfo+"',"+deliveryBoy+","+deliveryType+",now(),'"+paymentToken+"',"+grandTotal+","+mxId+")";
                Statement st = db.connection.createStatement();
                st.executeUpdate(q);

                FCMMessageSender.sendPushNotification(deliveryBoy ,"You Have a New Order to Deliver!");

                session.setAttribute("successMsg", "Success! Your Products is on the way to your doorstep.");
                response.sendRedirect("home");
            }

            String paymentToken = DeliveryUtil.getPaymentToken();

            List<String> productIdList = new ArrayList<>();
            List<String> productNameList = new ArrayList<>();
            List<Double> productPriceList = new ArrayList<>();
            List<Integer> productQuantityList = new ArrayList<>();

            int productCount = 0;

            while (request.getParameter("item_id_"+(productCount +1))!=null && !request.getParameter("item_id_"+(productCount +1)).equals("")) {
                productCount++;
                productIdList.add(request.getParameter("item_id_"+ productCount));
                productNameList.add(request.getParameter("item_name_"+ productCount));
                productPriceList.add(Double.valueOf(request.getParameter("amount_"+ productCount)));
                productQuantityList.add(Integer.valueOf(request.getParameter("quantity_"+ productCount)));
            }


        %>


        <!-- about -->
        <div class="privacy about">
            <h3>Chec<span>kout</span></h3>

            <div class="checkout-right">
                <h4>Your shopping cart contains: <span><%=productCount%> Products</span></h4>
                <table class="timetable_sub">
                    <thead>
                    <tr class="row">
                        <th>SL No.</th>
                        <th>Product Image</th>
                        <th>Quantity</th>
                        <th>Product Name</th>
                        <th>Unit Price</th>
                        <th>Total Price</th>
                        <%--<th>Remove</th>--%>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        Double subTotal=0.0;
                    for(int i=0;i<productCount;i++) {
                    %>
                    <tr class="row rem1">
                        <td class="col col-sm-1 invert"><%=(i+1)%></td>
                        <td class="col col-sm-2 invert-image"><a href="single?id=<%=productIdList.get(i)%>"><img src="resources/images/products/<%=productIdList.get(i)%>.jpg" alt=" " class="img-responsive"></a></td>
                        <td class="col col-sm-1 invert"><%=productQuantityList.get(i)%></td>
                        <%--<td class="col col-sm-2 invert">
                            <div class="quantity">
                                <div class="quantity-select">
                                    <div class="entry value-minus">&nbsp;</div>
                                    <div class="entry value"><span><%=productQuantityList.get(i)%></span></div>
                                    <div class="entry value-plus active">&nbsp;</div>
                                </div>
                            </div>
                        </td>--%>
                        <td class="col col-sm-4 invert"><%=productNameList.get(i)%></td>

                        <td class="col col-sm-2 invert">$<%=productPriceList.get(i)%></td>
                        <td class="col col-sm-2 invert">$<%=(productPriceList.get(i)*productQuantityList.get(i))%></td>
                        <%--<td class="invert">
                            <div class="rem">
                                <div class="close1"> </div>
                            </div>

                        </td>--%>
                    </tr>
                    <%
                        subTotal+=(productPriceList.get(i)*productQuantityList.get(i));
                    }
                    %>
                    <tr class="row">
                        <td class="col col-sm-1 invert"></td>
                        <td class="col col-sm-2 invert"></td>
                        <td class="col col-sm-1 invert"></td>
                        <td class="col col-sm-4 invert"></td>
                        <td class="col col-sm-2 invert"><b>SUBTOTAL :</b></td>
                        <td class="col col-sm-2 invert"><b>$<%=subTotal%></b></td>

                    </tr>
                    </tbody></table>
            </div>
            <div class="checkout-left">
                <%--<div class="col-md-4 checkout-left-basket">
                    <h4>Continue to basket</h4>
                    <ul>
                        <li>Product1 <i>-</i> <span>$15.00 </span></li>
                        <li>Product2 <i>-</i> <span>$25.00 </span></li>
                        <li>Product3 <i>-</i> <span>$29.00 </span></li>
                        <li>Total Service Charges <i>-</i> <span>$15.00</span></li>
                        <li>Total <i>-</i> <span>$84.00</span></li>
                    </ul>
                </div>--%>
                <div class="col-md-12 address_form_agile">
                    <h4>Delivery Details</h4>
                    <form action="checkout" method="post" class="creditly-card-form agileinfo_form">
                        <section class="creditly-wrapper wthree, w3_agileits_wrapper">
                            <div class="information-wrapper">
                                <div class="first-row form-group">
                                    <div class="controls">
                                        <label class="control-label">Full name: </label>
                                        <input class="billing-address-name form-control" type="text" name="name" placeholder="Full name" required>
                                    </div>
                                    <div class="w3_agileits_card_number_grids">
                                        <div class="w3_agileits_card_number_grid_left">
                                            <div class="controls">
                                                <label class="control-label">Mobile number:</label>
                                                <input class="form-control" type="text" name="mobile" placeholder="Mobile number" required>
                                            </div>
                                        </div>

                                        <div class="clear"> </div>
                                    </div>
                                    <div class="controls">
                                        <label class="control-label">Delivery Address: </label>
                                        <input class="form-control" type="text" name="address" placeholder="Delivery Address" required>
                                    </div>
                                    <%--<div class="controls">
                                        <label class="control-label">Delivery type: </label>
                                        <select class="form-control option-w3ls" name="deliveryType">
                                            <option><b>+50$</b> : Fastest (60 minutes delivery) </option>
                                            <option><b>+10$</b> : Normal (6 hours delivery)</option>
                                            <option><b>+5$</b> : Economy (24 hours delivery)</option>
                                        </select>
                                    </div>--%>
                                    <div class="controls">
                                        <label class="control-label">Payment Token: </label>
                                        <input class="form-control"  name="paymentToken" value="<%=paymentToken%>" readonly required>
                                        <b>Please Store This Token And Must Put This Token When Making The Payment In Square Cash</b>
                                    </div>
                                </div>

                                <%
                                for(int i=0;i<productCount;i++) {
                                    %>
                                <input type="hidden" name="productId_<%=(i+1)%>" value="<%=productIdList.get(i)%>">
                                <input type="hidden" name="productQuantity_<%=(i+1)%>" value="<%=productQuantityList.get(i)%>">
                                <%
                                }
                                %>

                                <input type="hidden" name="subTotal" value="<%=subTotal%>"/>
                                <div class="row">
                                    <div class="col col-sm-4"><input type="submit" name="deliveryType1" value="Confirm Fastest Delivery ($<%=subTotal+10%>)" class="btn btn-success"/></div>
                                    <div class="col col-sm-4"><input type="submit" name="deliveryType2" value="Confirm Normal Delivery ($<%=subTotal+5%>)" class="btn btn-info"/></div>
                                    <div class="col col-sm-4"><input type="submit" name="deliveryType3" value="Confirm Economy Delivery ($<%=subTotal+2%>)" class="btn btn-default"/></div>
                                </div>
                            </div>
                        </section>
                    </form>
                    <%--<div class="checkout-right-basket">
                        <a href="payment.html">Make a Payment <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </div>--%>
                </div>

                <div class="clearfix"> </div>

            </div>

        </div>
        <!-- //about -->
    </div>
    <div class="clearfix"></div>
</div>
<!-- //banner -->


<%--<!-- newsletter -->
<div class="newsletter">
    <div class="container">
        <div class="w3agile_newsletter_left">
            <h3>sign up for our newsletter</h3>
        </div>
        <div class="w3agile_newsletter_right">
            <form action="#" method="post">
                <input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                <input type="submit" value="subscribe now">
            </form>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!-- //newsletter -->--%>

<%
    } catch (Exception e) {
    e.printStackTrace();
        session.setAttribute("errorMsg", "Error! Please Try Again.");
        response.sendRedirect("home");


    } finally {
    db.close();
    }
%>
<%@ include file="footer.jsp" %>
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!--quantity-->
<script>
    $('.value-plus').on('click', function(){
        var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)+1;
        divUpd.text(newVal);
    });

    $('.value-minus').on('click', function(){
        var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)-1;
        if(newVal>=1) divUpd.text(newVal);
    });
</script>
<!--quantity-->
<script>$(document).ready(function(c) {
    $('.close1').on('click', function(c){
        $('.rem1').fadeOut('slow', function(c){
            $('.rem1').remove();
        });
    });
});
</script>
<script>$(document).ready(function(c) {
    $('.close2').on('click', function(c){
        $('.rem2').fadeOut('slow', function(c){
            $('.rem2').remove();
        });
    });
});
</script>
<script>$(document).ready(function(c) {
    $('.close3').on('click', function(c){
        $('.rem3').fadeOut('slow', function(c){
            $('.rem3').remove();
        });
    });
});
</script>

<!-- //js -->
<!-- script-for sticky-nav -->
<script>
    $(document).ready(function() {
        var navoffeset=$(".agileits_header").offset().top;
        $(window).scroll(function(){
            var scrollpos=$(window).scrollTop();
            if(scrollpos >=navoffeset){
                $(".agileits_header").addClass("fixed");
            }else{
                $(".agileits_header").removeClass("fixed");
            }
        });

    });
</script>
<!-- //script-for sticky-nav -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
        $(".dropdown").hover(
            function() {
                $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
                $(this).toggleClass('open');
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
                $(this).toggleClass('open');
            }
        );
    });
</script>
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear'
         };
         */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- //here ends scrolling icon -->
<script src="js/minicart.js"></script>
<script>
    paypal.minicart.render();

    paypal.minicart.cart.on('checkout', function (evt) {
        var items = this.items(),
            len = items.length,
            total = 0,
            i;

        // Count the number of each item in the cart
        for (i = 0; i < len; i++) {
            total += items[i].get('quantity');
        }

        if (total < 3) {
            alert('The minimum order quantity is 3. Please add more to your shopping cart before checking out');
            evt.preventDefault();
        }
    });

</script>
</body>
</html>