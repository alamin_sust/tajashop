<%--
    Document   : updateSoldierLocation
    Created on : Feb 25, 2017, 9:36:59 PM
    Author     : Al-Amin
--%>

<%@page import="java.sql.Date"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.nio.file.Paths"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.tajashop.connection.Database"%>

<%
    // Returns all employees (active and terminated) as json.
    Database db = new Database();
    db.connect();
    try{

        Statement st2 = db.connection.createStatement();
        String mxIdQry = "select max(id)+1 as mxid from delivery_boy_location";
        ResultSet rs2 = st2.executeQuery(mxIdQry);

        rs2.next();



        /*Statement st3 = db.connection.createStatement();
        String mxPathIdQry = "select max(path_id)+1 as mxpathid from delivery_boy_location";
        ResultSet rs3 = st3.executeQuery(mxPathIdQry);

        rs3.next();*/

        String pathId = new String();


        if(request.getParameter("startPath")!=null) {
            Statement st4 = db.connection.createStatement();
            String qry4 = "UPDATE delivery_boy set path_status=1 where id="+request.getParameter("id");
            st4.executeUpdate(qry4);
        }

        /*if(request.getParameter("pathId")==null) {
            pathId = rs3.getString("mxpathid");
            Statement st4 = db.connection.createStatement();
            String qry4 = "UPDATE delivery_boy set path_status=1 where id="+request.getParameter("id");

            st4.executeUpdate(qry4);
        }
        else {*/
            pathId = request.getParameter("pathId");
        //}


        Statement stRegMob = db.connection.createStatement();
        String qRegMob = "select * from registered_mobiles where substr(imie_number,1,15) like '"+request.getParameter("authImie").replace("/","").substring(0,15)+"'";
        ResultSet rsRegMob = stRegMob.executeQuery(qRegMob);
        rsRegMob.next();



        String arrLongitude[] = request.getParameter("longitude").split("-");
        String arrLatitude[] = request.getParameter("latitude").split("-");



        for(int i=0;i<arrLongitude.length;i++) {
            Statement st = db.connection.createStatement();
            String qry = "insert into delivery_boy_location(id,delivery_boy_id,longitude,latitude,datetime,path_id,imie_number) values(" + rs2.getString("mxid") + "," + request.getParameter("id")
                    + ", '" + arrLongitude[i] + "','" + arrLatitude[i] + "',now()," + pathId + ",'" +  request.getParameter("authImie") + "' )";

            String qryLastLoc = "update delivery_boy set last_longitude='" + arrLongitude[i] + "', last_latitude='" + arrLatitude[i] + "',  datetime=now() where id=" + request.getParameter("id");
            Statement stLastLoc = db.connection.createStatement();
            stLastLoc.executeUpdate(qryLastLoc);
            st.executeUpdate(qry);
        }

        if(request.getParameter("endPath")!=null) {
            Statement st4 = db.connection.createStatement();
            String qry4 = "UPDATE delivery_boy set path_status=0 where id="+request.getParameter("id");
            st4.executeUpdate(qry4);

            Statement st5 = db.connection.createStatement();
            String qry5 = "UPDATE order_ set status='completed' where path_id="+pathId;
            st5.executeUpdate(qry5);
        }

%>
{"result":"1",
"pathId":"<%=pathId%>"
}
<%
}
catch(Exception e){
%>
{"result":"0"}
<%
    } finally {
        db.close();
    }


%>
