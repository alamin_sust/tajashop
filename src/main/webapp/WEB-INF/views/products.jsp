<%@ page import="java.sql.ResultSet" %>
<%@ page import="com.tajashop.connection.Database" %>

<%@page import="java.sql.Statement"%>
<%@page import="com.tajashop.connection.Database"%>
<%@ include file="header.jsp" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!-- banner -->
<div class="banner_bottom">
    <div class="wthree_banner_bottom_left_grid_sub">
    </div>
    <div class="wthree_banner_bottom_left_grid_sub1">
        <div class="col-md-4 wthree_banner_bottom_left">
            <div class="wthree_banner_bottom_left_grid">
                <img src="resources/images/4.jpg" alt=" " class="img-responsive"/>
                <div class="wthree_banner_bottom_left_grid_pos">
                    <h4>Discount Offer <span>25%</span></h4>
                </div>
            </div>
        </div>
        <div class="col-md-4 wthree_banner_bottom_left">
            <div class="wthree_banner_bottom_left_grid">
                <img src="resources/images/5.jpg" alt=" " class="img-responsive"/>
                <div class="wthree_banner_btm_pos">
                    <h3>introducing <span>best store</span> for <i>groceries</i></h3>
                </div>
            </div>
        </div>
        <div class="col-md-4 wthree_banner_bottom_left">
            <div class="wthree_banner_bottom_left_grid">
                <img src="resources/images/6.jpg" alt=" " class="img-responsive"/>
                <div class="wthree_banner_btm_pos1">
                    <h3>Save <span>Upto</span> $10</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<%

    Database db = new Database();
    db.connect();

    try {

        String searchHeader = "Products: ";

        String category = request.getParameter("category");
        String subCategory = request.getParameter("subCategory");

        String q = "select * from category where id>0 and name is not null ";


        if (category != null && !category.equals("")) {
            q += " and category_number=" + category;
        }
        if (subCategory != null && !subCategory.equals("")) {
            q += " and subcategory_number=" + subCategory;
        }

        Statement st  = db.connection.createStatement();
        ResultSet rs = st.executeQuery(q);

        if((category != null && !category.equals("")) || (subCategory != null && !subCategory.equals(""))) {
        if(rs.next()) {
            searchHeader+=rs.getString("name");
            rs.beforeFirst();
        }
        } else if (request.getParameter("searchProduct")!=null){
            searchHeader+=" Search Results of : "+request.getParameter("searchProduct");
        }
        else {
            searchHeader="Products: All Products";
        }



%>
<!-- halal-food -->
<div class="top-brands">
    <div class="container">
        <h3><%=searchHeader%></h3>
        <div class="agile_top_brands_grids">
            <%
                while(rs.next()) {


                   String q2="select * from product where category_id="+rs.getString("id");

                   if(request.getParameter("searchProduct")!=null) {
                       q2="select * from product where LOWER(name) like LOWER ('%"+request.getParameter("searchProduct")+"%')";
                   }

                   Statement st2 = db.connection.createStatement();
                   ResultSet rs2 = st2.executeQuery(q2);
                   while (rs2.next()) {

            %>
            <div class="col-md-3 top_brand_left">
                <div class="hover14 column">
                    <div class="agile_top_brand_left_grid">
                        <div class="tag"><a href="single?id=<%=rs2.getString("id")%>"><img src="resources/images/products/<%=rs2.getString("id")%>.jpg" alt=" " class="img-responsive"/></a></div>
                        <div class="agile_top_brand_left_grid1">
                            <figure>
                                <div class="snipcart-item block">
                                    <div class="snipcart-thumb">
                                        <br><br><br><br><br><br><br>
                                        <p><%=rs2.getString("name")%></p>
                                        <h4>$<%=rs2.getString("price")%> <%--<span>$10.00</span>--%></h4>
                                    </div>
                                    <div class="snipcart-details top_brand_home_details">
                                        <%if(session.getAttribute("username")!=null
                                                && session.getAttribute("username").equals("admin")) {%>
                                        <form action="addProduct" method="get">
                                            <input type="hidden" name="updateProduct" value="<%=rs2.getString("id")%>">
                                            <input type="submit" name="submit" value="Update Product" class="button"/>
                                        </form>
                                        <%}else{%>
                                        <form action="checkout" method="post">
                                            <fieldset>
                                                <input type="hidden" name="item_id" value="<%=rs2.getString("id")%>"/>
                                                <input type="hidden" name="cmd" value="_cart"/>
                                                <input type="hidden" name="add" value="1"/>
                                                <input type="hidden" name="business" value=" "/>
                                                <input type="hidden" name="item_name" value="<%=rs2.getString("name")%>"/>
                                                <input type="hidden" name="amount" value="<%=rs2.getString("price")%>"/>
                                                <input type="hidden" name="discount_amount" value="0.00"/>
                                                <input type="hidden" name="currency_code" value="USD"/>
                                                <input type="hidden" name="return" value=" "/>
                                                <input type="hidden" name="cancel_return" value=" "/>
                                                <input type="submit" name="submit" value="Add to cart" class="button"/>
                                                <br>
                                                <a class="btn btn-info" href="single?id=<%=rs2.getString("id")%>">Product Details</a>
                                            </fieldset>
                                        </form>
                                            <%}%>


                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <%}
                if(request.getParameter("searchProduct")!=null) {
                       break;
                }
                }%>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //halal food-->

<%
    } catch (Exception e) {
        e.printStackTrace();
    } finally {
        db.close();
    }
%>


<%@ include file="footer.jsp" %>
