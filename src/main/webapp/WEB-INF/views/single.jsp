<%@page import="java.io.DataInputStream" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="com.tajashop.connection.Database" %>
<%@page import="java.io.OutputStream" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.File" %>
<%@page import="java.io.InputStream" %>
<%@ page import="com.sun.org.apache.regexp.internal.RE" %>


<%@ include file="header.jsp" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%

    Database db = new Database();
    db.connect();

    try {

        String q2 = "select * from product where id="+request.getParameter("id");


        Statement st2  = db.connection.createStatement();
        ResultSet rs2 = st2.executeQuery(q2);
        rs2.next();

%>

    <div class="w3l_banner_nav_right">
        <div class="w3l_banner_nav_right_banner3">
            <h3>Best Deals For New Products<span class="blink_me"></span></h3>
        </div>
        <div class="agileinfo_single">
            <h5><%=rs2.getString("name")%></h5>
            <div class="col-md-4 agileinfo_single_left">
                <img id="example" src="resources/images/products/<%=rs2.getString("id")%>.jpg" alt=" " class="img-responsive" />
            </div>
            <div class="col-md-8 agileinfo_single_right">
                <%--<div class="rating1">
						<span class="starRating">
							<input id="rating5" type="radio" name="rating" value="5">
							<label for="rating5">5</label>
							<input id="rating4" type="radio" name="rating" value="4">
							<label for="rating4">4</label>
							<input id="rating3" type="radio" name="rating" value="3" checked>
							<label for="rating3">3</label>
							<input id="rating2" type="radio" name="rating" value="2">
							<label for="rating2">2</label>
							<input id="rating1" type="radio" name="rating" value="1">
							<label for="rating1">1</label>
						</span>
                </div>--%>
                <div class="w3agile_description">
                    <h4>Description :</h4>
                    <p><%=rs2.getString("description")%></p>
                </div>
                <div class="snipcart-item block">
                    <div class="snipcart-thumb agileinfo_single_right_snipcart">
                        <h4>$<%=rs2.getString("price")%></h4>
                    </div>
                    <div class="snipcart-details agileinfo_single_right_details">
                        <form action="#" method="post">
                            <fieldset>
                                <input type="hidden" name="item_id" value="<%=rs2.getString("id")%>"/>
                                <input type="hidden" name="cmd" value="_cart"/>
                                <input type="hidden" name="add" value="1"/>
                                <input type="hidden" name="business" value=" "/>
                                <input type="hidden" name="item_name" value="<%=rs2.getString("name")%>"/>
                                <input type="hidden" name="amount" value="<%=rs2.getString("price")%>"/>
                                <input type="hidden" name="discount_amount" value="0.00"/>
                                <input type="hidden" name="currency_code" value="USD"/>
                                <input type="hidden" name="return" value=" "/>
                                <input type="hidden" name="cancel_return" value=" "/>
                                <input type="submit" name="submit" value="Add to cart" class="button"/>
                                <br>
                                <%--<a class="btn btn-info" href="single?id=<%=rs2.getString("id")%>">Product Details</a>--%>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- //banner -->
<%
    } catch (Exception e) {
        e.printStackTrace();
    } finally {
        db.close();
    }
%>
<%@ include file="footer.jsp" %>
