<%--
    Document   : updateSoldierLocation
    Created on : Feb 25, 2017, 9:36:59 PM
    Author     : Al-Amin
--%>

<%@page import="java.sql.Date"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.nio.file.Paths"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.tajashop.connection.Database"%>
<%@ page import="java.sql.Timestamp" %>

<%
        // Returns all employees (active and terminated) as json.
        Database db = new Database();
        db.connect();
        try{

            Statement st = db.connection.createStatement();
            String q = "select * from order_ where delivery_boy_id="+request.getParameter("deliveryBoyId")+" and status='running'";
            ResultSet rs = st.executeQuery(q);

            rs.next();

            int ONE_HOUR = (1000*60*60);
            int time;

            if(rs.getString("delivery_type").equals("1")) {
                time = ONE_HOUR;
            } else if(rs.getString("delivery_type").equals("2")) {
                time = 6*ONE_HOUR;
            } else {
                time = 24*ONE_HOUR;
            }

            Timestamp deadline = new Timestamp(rs.getTimestamp("datetime").getTime() + time);




%>
{"result":"1",
"name":"<%=rs.getString("name")%>",
"mobile":"<%=rs.getString("mobile")%>",
"address":"<%=rs.getString("address")%>",
"deadline":"<%=deadline%>",
"products":"<%=rs.getString("product_info")%>",
"pathId":"<%=rs.getString("path_id")%>",
"grandTotal":"<%=rs.getString("grand_total")%>"
}
<%
}
catch(Exception e){
%>
{"result":"0"}
<%
    } finally {
        db.close();
    }


%>
