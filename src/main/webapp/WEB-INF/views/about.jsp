<%@page import="java.io.DataInputStream" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="com.tajashop.connection.Database" %>
<%@page import="java.io.OutputStream" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.File" %>
<%@page import="java.io.InputStream" %>
<%@ page import="com.sun.org.apache.regexp.internal.RE" %>
<%@ page import="static com.tajashop.web.util.EncryptionUtil.matchWithSecuredHash" %>
<%@ page import="static com.tajashop.web.util.EncryptionUtil.generateSecuredHash" %>


<%@ include file="header.jsp" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div class="w3l_banner_nav_right">
        <!-- about -->
        <div class="privacy about">
            <h3>About Us</h3>
            <p class="animi">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias
                excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui
                officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem
                rerum facilis est et expedita distinctio.</p>
            <div class="agile_about_grids">
                <div class="col-md-6 agile_about_grid_right">
                    <img src="images/31.jpg" alt=" " class="img-responsive" />
                </div>
                <div class="col-md-6 agile_about_grid_left">
                    <ol>
                        <li>laborum et dolorum fuga</li>
                        <li>corrupti quos dolores et quas</li>
                        <li>est et expedita distinctio</li>
                        <li>deleniti atque corrupti quos</li>
                        <li>excepturi sint occaecati cupiditate</li>
                        <li>accusamus et iusto odio</li>
                    </ol>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!-- //about -->
    </div>
    <div class="clearfix"></div>
</div>
<!-- //banner -->
<!-- team -->
<div class="team">
    <div class="container">
        <h3>Meet Our Amazing Team</h3>
        <div class="agileits_team_grids">
            <div class="col-md-3 agileits_team_grid">
                <img src="images/32.jpg" alt=" " class="img-responsive" />
                <h4>Martin Paul</h4>
                <p>Manager</p>
                <ul class="agileits_social_icons agileits_social_icons_team">
                    <li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="col-md-3 agileits_team_grid">
                <img src="images/33.jpg" alt=" " class="img-responsive" />
                <h4>Michael Rick</h4>
                <p>Supervisor</p>
                <ul class="agileits_social_icons agileits_social_icons_team">
                    <li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="col-md-3 agileits_team_grid">
                <img src="images/34.jpg" alt=" " class="img-responsive" />
                <h4>Thomas Carl</h4>
                <p>Supervisor</p>
                <ul class="agileits_social_icons agileits_social_icons_team">
                    <li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="col-md-3 agileits_team_grid">
                <img src="images/35.jpg" alt=" " class="img-responsive" />
                <h4>Laura Lee</h4>
                <p>CEO</p>
                <ul class="agileits_social_icons agileits_social_icons_team">
                    <li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //team -->
<!-- testimonials -->
<div class="testimonials">
    <div class="container">
        <h3>Testimonials</h3>
        <div class="w3_testimonials_grids">
            <div class="wmuSlider example1 animated wow slideInUp" data-wow-delay=".5s">
                <div class="wmuSliderWrapper">
                    <article style="position: absolute; width: 100%; opacity: 0;">
                        <div class="banner-wrap">
                            <div class="col-md-6 w3_testimonials_grid">
                                <p><i class="fa fa-quote-right" aria-hidden="true"></i>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis
                                    voluptatibus maiores alias consequatur aut perferendis doloribus asperiores
                                    repellat.</p>
                                <h4>Andrew Smith <span>Customer</span></h4>
                            </div>
                            <div class="col-md-6 w3_testimonials_grid">
                                <p><i class="fa fa-quote-right" aria-hidden="true"></i>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis
                                    voluptatibus maiores alias consequatur aut perferendis doloribus asperiores
                                    repellat.</p>
                                <h4>Thomson Richard <span>Customer</span></h4>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </article>
                    <article style="position: absolute; width: 100%; opacity: 0;">
                        <div class="banner-wrap">
                            <div class="col-md-6 w3_testimonials_grid">
                                <p><i class="fa fa-quote-right" aria-hidden="true"></i>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis
                                    voluptatibus maiores alias consequatur aut perferendis doloribus asperiores
                                    repellat.</p>
                                <h4>Crisp Kale <span>Customer</span></h4>
                            </div>
                            <div class="col-md-6 w3_testimonials_grid">
                                <p><i class="fa fa-quote-right" aria-hidden="true"></i>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis
                                    voluptatibus maiores alias consequatur aut perferendis doloribus asperiores
                                    repellat.</p>
                                <h4>John Paul <span>Customer</span></h4>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </article>
                    <article style="position: absolute; width: 100%; opacity: 0;">
                        <div class="banner-wrap">
                            <div class="col-md-6 w3_testimonials_grid">
                                <p><i class="fa fa-quote-right" aria-hidden="true"></i>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis
                                    voluptatibus maiores alias consequatur aut perferendis doloribus asperiores
                                    repellat.</p>
                                <h4>Rosy Carl <span>Customer</span></h4>
                            </div>
                            <div class="col-md-6 w3_testimonials_grid">
                                <p><i class="fa fa-quote-right" aria-hidden="true"></i>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis
                                    voluptatibus maiores alias consequatur aut perferendis doloribus asperiores
                                    repellat.</p>
                                <h4>Rockson Doe <span>Customer</span></h4>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </article>
                </div>
            </div>
            <script src="js/jquery.wmuSlider.js"></script>
            <script>
                $('.example1').wmuSlider();
            </script>
        </div>
    </div>
</div>
<!-- //testimonials -->
<%@ include file="footer.jsp" %>