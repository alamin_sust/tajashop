<%@page import="java.io.DataInputStream" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="com.tajashop.connection.Database" %>
<%@page import="java.io.OutputStream" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.File" %>
<%@page import="java.io.InputStream" %>
<%@ page import="com.sun.org.apache.regexp.internal.RE" %>


<%@ include file="header.jsp" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%
    if (session.getAttribute("username") == null || !session.getAttribute("username").equals("admin")) {
        response.sendRedirect("home");
    }

    Database db = new Database();
    db.connect();
    try{

    String id ="";
    String name = "";
    String description = "";
    String price = "";
    String remainingQty = "";
    String categoryId = "";

    if ((request.getParameter("updateProduct") != null && !request.getParameter("updateProduct").equals(""))
            || (session.getAttribute("updateProduct") != null && !session.getAttribute("updateProduct").equals(""))) {

        id = request.getParameter("updateProduct");

        if (session.getAttribute("updateProduct") != null && !session.getAttribute("updateProduct").equals("")) {
            id = session.getAttribute("updateProduct").toString();
            session.setAttribute("updateProduct",null);
        }

        String q3 = "select * from product where id=" + id;
        Statement st3 = db.connection.createStatement();
        ResultSet rs3 = st3.executeQuery(q3);
        rs3.next();

        name = rs3.getString("name");
        description = rs3.getString("description");
        price = rs3.getString("price");
        remainingQty = rs3.getString("remaining_qty");
        categoryId = rs3.getString("category_id");

    }

%>


<div class="w3l_banner_nav_right">

    <div class="col col-sm-10 col-sm-offset-1">
        <%if (session.getAttribute("successMsg") != null && !session.getAttribute("successMsg").equals("")) {%>
        <div class="alert alert-success"><%=session.getAttribute("successMsg")%>
        </div>
        <%}%>
        <%
            session.setAttribute("successMsg", null);
        %>


        <form method="POST" action="addProduct" enctype="multipart/form-data">
            <tr>
                <td>Name</td>
                <td><input name="name" class="form-control" type="text" value="<%=name%>" required/></td>
            </tr>
            <br>
            <tr>
                <td>Description</td>
                <td><input name="description" type="text" id="" value="<%=description%>" class="form-control" required/>
                </td>
            </tr>
            <br>
            <tr>
                <td>Price</td>
                <td><input name="price" type="text" class="aksTA form-control" value="<%=price%>" maxlength="500"
                           required></td>
            </tr>
            <br>
            <tr>
                <td>Quantity to Inventory</td>
                <td><input name="remainingQty" type="text" class="aksTA form-control" value="<%=remainingQty%>"
                           maxlength="500" required></td>
            </tr>
            <br>
            <tr>
                <td>Category & Sub-Category</td>
                <td>
                    <select class="form-control" name="categoryId" required>
                        <option value="">--Please Select--</option>
                        <%
                            String q3 = "select category_number,COUNT(*) cnt from category GROUP BY category_number";
                            Statement st3 = db.connection.createStatement();
                            ResultSet rs3 = st3.executeQuery(q3);
                            while (rs3.next()) {

                                String q4 = "select * from category WHERE category_number=" + rs3.getString("category_number");
                                Statement st4 = db.connection.createStatement();
                                ResultSet rs4 = st4.executeQuery(q4);

                                while (rs4.next()) {
                                    if (rs3.getInt("cnt") > 1 && rs4.getInt("subcategory_number") == 0) {

                        %>
                        <option disabled><b><%=rs4.getString("name")%>
                        </b>-->
                        </option>
                        <%
                                continue;
                            }

                        %>
                        <option value="<%=rs4.getString("id")%>"
                                <%if(rs4.getString("id").equals(categoryId)){%>selected<%}%> ><%=rs4.getString("name")%>
                        </option>
                        <%
                                }
                            }
                        %>
                    </select>
                </td>
            </tr>
            <br>
            <%if (!id.equals("")) {%>
            <input type="hidden" name="updateProduct" value="<%=id%>">
            <%} else {%>
            <input type="hidden" name="insertProduct" value="true">
            <%}%>

            <%--<input name="selectedId" value="<%=id%>" type="hidden">--%>

            <tr>
                <td>Product Image</td>
                <td><input type="file" name="file" value=""/></td>
            </tr>


            <%--<tr>
                <td></td>
                <td><input type="submit" value="UPLOAD" name="submit" required/></td>
                <td><img src="resources/images/products/<%=id%>.jpg" width="150px;" height="100px;" class="img-responsive img-circle center-block" /></td>
            </tr>--%>
            <br>
            <button type="submit" id="btnSaveChanges" class="btn btn-success"><%if(id.equals("")){%>Insert<%}else{%>Update<%}%><span class="fa fa-save"></span>
            </button>
        </form>
    </div>

</div>

<div class="clearfix"></div>
</div>
<!-- //banner -->
<!-- newsletter-top-serv-btm -->
<div class="newsletter-top-serv-btm">
    <div class="container">
        <div class="col-md-4 wthree_news_top_serv_btm_grid">
            <div class="wthree_news_top_serv_btm_grid_icon">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            </div>
            <h3>Nam libero tempore</h3>
            <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus
                saepe eveniet ut et voluptates repudiandae sint et.</p>
        </div>
        <div class="col-md-4 wthree_news_top_serv_btm_grid">
            <div class="wthree_news_top_serv_btm_grid_icon">
                <i class="fa fa-bar-chart" aria-hidden="true"></i>
            </div>
            <h3>officiis debitis aut rerum</h3>
            <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus
                saepe eveniet ut et voluptates repudiandae sint et.</p>
        </div>
        <div class="col-md-4 wthree_news_top_serv_btm_grid">
            <div class="wthree_news_top_serv_btm_grid_icon">
                <i class="fa fa-truck" aria-hidden="true"></i>
            </div>
            <h3>eveniet ut et voluptates</h3>
            <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus
                saepe eveniet ut et voluptates repudiandae sint et.</p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<%
    }catch (Exception e) {

    } finally {
            db.close();
    }
%>
<!-- //newsletter-top-serv-btm -->
<%@ include file="footer.jsp" %>














