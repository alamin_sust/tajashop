<%@ taglib prefix="b" uri="http://www.springframework.org/tags/form" %>
<%--
    Document   : currentLocation
    Created on : Feb 25, 2017, 2:17:08 AM
    Author     : Al-Amin
--%>

<%@page import="com.tajashop.connection.Database" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.ArrayList" %>
<%@ page import="com.tajashop.web.util.Constants" %>
<%@ page import="java.sql.Struct" %>

<%@ include file="header.jsp" %>

    <style>
        #map {
            height: 600px;
            width: 70%;
        }
    </style>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"
            type="text/javascript"></script>

    <%
        if(request.getParameter("pathId")==null || request.getParameter("pathId").equals("")) {
            response.sendRedirect("home");
        }
        Database db = new Database();
        db.connect();
        try {

            String pathId = request.getParameter("pathId");
                Statement st = db.connection.createStatement();
                String q="select * from delivery_boy_location where path_id="+pathId+" order by id DESC ";
                ResultSet rs = st.executeQuery(q);

                if(rs.next()) {



                String datetime;
                String longitude;
                String latitude;

            datetime = rs.getString("datetime");
            longitude = rs.getString("longitude");
            latitude = rs.getString("latitude");




            %>



    <h3>My Order Status</h3>
    <div id="map"></div>
    <script type="text/javascript">

        var longitude = <%=longitude%>;
        var latitude = <%=latitude%>;
        var datetime = '<%=datetime%>';
        var pathId = '<%=pathId%>';

        var centerPointX = latitude;
        var centerPointY = longitude;
        var zoom = 13;

        function initMap() {
            var uluru = {lat: centerPointX, lng: centerPointY};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: zoom,
                center: uluru
            });
            /*var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });*/
            var infowindow = new google.maps.InfoWindow();

            (function(){

                var status = "n/a";
                var image = "n/a";
                var label = "n/a";
                var markerPoint = "";

                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(centerPointX, centerPointY),
                    animation: google.maps.Animation.DROP,
                    map: map,
                    label: "Delivery Boy Currently Here"
                });



                google.maps.event.addListener(marker, 'click', (function (marker) {
                    return function () {

                        infowindow.setContent("Time: " + datetime + "<br>Latitude: " + latitude + "<br>Longitude: " + longitude + "<br>Path ID: " + pathId + "</b>");

                        infowindow.open(map, marker);

                    }
                })(marker));

            })();
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASpnPqQS-IcKvS0KQYP_G10lk3IU9RoCE&callback=initMap">
    </script>




<%} else {
    session.setAttribute("errorMsg", "Delivery Has Not Started!");
    response.sendRedirect("orders");
}
    }catch (Exception e){
        response.sendRedirect("somethingWentWrong");
    }finally {
        db.close();
    }


%>
<%@ include file="footer.jsp" %>


